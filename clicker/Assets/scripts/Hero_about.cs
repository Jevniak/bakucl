﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hero_about : MonoBehaviour {
 
    public int hero_id;
    public static Hero_about self;

    public void Show_about(int hid)
    {
        hero_id = hid;
        
        MainScript.ms.fon_heroes_one.SetActive(true);

        Hero hero = MainScript.ms.player.data.Heros.Find(x => x.type == hero_id.ToString());

        //int curent_lvl = 0;
        //for (curent_lvl = 0; curent_lvl < MainScript.ms.lvl_up_val.Count && MainScript.ms.lvl_up_val[curent_lvl] <= MainScript.ms.player.data.Heros[hero_id].damageAll; curent_lvl++) ;
        if (MainScript.ms.lvl_up_val[hero.lvl-1] < hero.damageAll) hero.damageAll = MainScript.ms.lvl_up_val[hero.lvl-1];
        //MainScript.ms.player.data.lvl = (curent_lvl + 1);

        GameObject.Find("her_image").GetComponent<Image>().sprite = Resources.Load<Sprite>("her_minico" + hero.type);
        GameObject.Find("her_name").GetComponent<Text>().text = hero.name;
        GameObject.Find("LEVEL_HERO").GetComponent<Text>().text = hero.lvl+"";
        GameObject.Find("her_heals").GetComponent<Text>().text = "ЗДОРОВЬЕ: " + hero.maxHp;
        GameObject.Find("her_opit").GetComponent<Text>().text = "ОПЫТ: "+hero.damageAll + "/" + MainScript.ms.lvl_up_val[hero.lvl-1] ;
        GameObject.Find("her_uron").GetComponent<Text>().text = "УРОН: " + hero.damage ;
        GameObject.Find("her_krit").GetComponent<Text>().text = "КРИТ. УДАР: "+ hero.crit_persent + " %";
        GameObject.Find("desc").GetComponent<Text>().text = MainScript.ms.all_description[System.Convert.ToInt32(hero.type)-1];
        //Debug.Log("lvl "+hero.lvl);

        //GameObject.Find("button_heal").GetComponentInChildren<Text>().text = "Вылечить " + System.Convert.ToInt32 ((hero.maxHp - hero.heals)/20) + " GOLD";

    }


    // Use this for initialization
    void Awake()
    {
        self = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
