﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class DragHandeler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{


    Vector3 startPosition;
    Transform startParent;

    public void OnBeginDrag(PointerEventData eventData)
    {

        if (MainScript.ms.fon_fight_prepare.activeSelf && !MainScript.ms.Find)
        {
             
            
                startPosition = transform.position;
                startParent = transform.parent;
                GetComponent<Image>().raycastTarget = false;
            
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (MainScript.ms.fon_fight_prepare.activeSelf && !MainScript.ms.Find)
        {
            transform.position = Input.mousePosition;
            transform.parent = MainScript.ms.fon_fight_prepare.transform;
            if (!MainScript.ms.selected)
            {
                MainScript.ms.selected = true;
                MainScript.ms.Minlvl = gameObject.GetComponent<Hero_script>().hero.lvl - 2;
                MainScript.ms.Maxlvl = gameObject.GetComponent<Hero_script>().hero.lvl + 2;
            }
            if (MainScript.ms.selected)
            {
                if (MainScript.ms.raiting)
                foreach (Transform n in GameObject.Find("fon_fight_prepare_heroes").transform)
                {
                    if (Convert.ToInt32(n.GetComponent<Hero_script>().hero.lvl) > MainScript.ms.Maxlvl || Convert.ToInt32(n.GetComponent<Hero_script>().hero.lvl) < MainScript.ms.Minlvl )
                        n.gameObject.SetActive(false);
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (MainScript.ms.fon_fight_prepare.activeSelf && !MainScript.ms.Find)
        {
            GetComponent<Image>().raycastTarget = true;

            var g = eventData.pointerEnter;
            int c = 1;
            if ( g.transform.parent.name.Contains("slot"))
            {
                Debug.Log(startParent);
                    
                    transform.parent = g.transform.parent.transform;
                g.gameObject.transform.parent = startParent;
                c = 0;
                
            }
            if (g.name.Contains("slot"))
            {
                if (g.GetComponentInChildren<Hero_script>() == null)
                {
                    transform.parent = g.transform;
                    c = 0;
                   
                }
                
            }

            if (c == 1)
            {

                transform.parent = GameObject.Find("fon_fight_prepare_heroes").transform;
                transform.position = startPosition;
                if (GameObject.Find("slot1").transform.childCount == 0 && GameObject.Find("slot2").transform.childCount == 0 && GameObject.Find("slot3").transform.childCount == 0)
                {
                    MainScript.ms.selected = false;
                    foreach (Transform n in GameObject.Find("fon_fight_prepare_heroes").transform)
                    {
                       n.gameObject.SetActive(true);
                    }
                }
            }
            if (
                         GameObject.Find("slot1").GetComponentInChildren<Hero_script>()
                         && GameObject.Find("slot2").GetComponentInChildren<Hero_script>()
                         && GameObject.Find("slot3").GetComponentInChildren<Hero_script>()

                         )
            {
                MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().color = new Color(1, 1, 1);
            }
            else
            {
                MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().color = new Color(0.5f, 0.5f, 0.5f);

            }

        }
    }


}
