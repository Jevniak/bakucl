﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBuy : MonoBehaviour
{
    public Shop_item cur_shop_item;
    public string art;
    public int cost;

    public GameObject objForDest;

    public string name;
    public string type;
    public int lvl;
    public int heals;
    public int damage;
    public float crit_persent;
    public float dodge_persent;
    public int maxHp;
    public int damageAll;
    public string description;

    public void buy_item()
    {
        if (cost <= MainScript.ms.player.data.gold)
        {

            MainScript.ms.player.data.Heros.Add(new Hero
            {
                name = name,
                type = type,
                lvl = lvl,
                heals = heals,
                damage = damage,
                crit_persent = crit_persent,
                dodge_persent = dodge_persent,
                maxHp = maxHp,
                damageAll = damageAll
                //description = description

            });




            MainScript.ms.player.data.gold -= cost;
            MainScript.ms.player.data.shop_items.Remove(cur_shop_item);

            StartCoroutine(MainScript.ms.UpdateInfo());

            Destroy(objForDest);
            MainScript.ms.fon_heroes_buy_window.SetActive(false);

        }
        // 

        MainScript.ms.Player_data_save();
    }




}
