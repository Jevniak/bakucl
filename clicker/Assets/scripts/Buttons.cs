﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using System.Net;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


using UnityEngine.EventSystems;
using UnityEngine.Events;


public class Buttons : MonoBehaviour, IPointerClickHandler
{


    void Start()
    {


    }
    void Update()
    {

    }



    public void PlayerTapSkill(int NumberFight_her)
    {


        if (GameObject.Find("fight_her1").GetComponent<Image>().color.a !=0 && MainScript.ms.cooldown > 70 && MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == MainScript.ms.player.data.selected_heroes[NumberFight_her].ToString()))].heals > 0)
        {

            MainScript.ms.LastClick = MainScript.ms.Get_enemy_alive_hero().name;
            MainScript.ms.Send_action("skill" + MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == MainScript.ms.player.data.selected_heroes[NumberFight_her].ToString()))].type, "0");
            MainScript.ms.cooldown -= 70;
        }

    }


    IEnumerator Animate_action()
    {
        Hero h = MainScript.ms.Get_enemy_alive_hero();

        if (h != null)
            for (int i = 1; i < 10; i++)
            {
                if (h.type == "0")
                {
                    GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s" + 7 + "/0" + i);
                }
                else
                {
                    GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s" + h.type + "/0" + i);
                }

                yield return new WaitForSeconds(0.01f);
            }

    }

    public void changeColorButtonMain(string nameButton)
    {
        MainScript.ms.fon.transform.Find("button_main").gameObject.GetComponent<Image>().sprite = MainScript.ms.button1_normal;
        MainScript.ms.fon.transform.Find("button_heroes").gameObject.GetComponent<Image>().sprite = MainScript.ms.button23_normal;
        MainScript.ms.fon.transform.Find("button_invent").gameObject.GetComponent<Image>().sprite = MainScript.ms.button23_normal;
        MainScript.ms.fon.transform.Find("button_shop").gameObject.GetComponent<Image>().sprite = MainScript.ms.button4_normal;

        if (nameButton == "button_shop")
            MainScript.ms.fon.transform.Find("button_shop").gameObject.GetComponent<Image>().sprite = MainScript.ms.button4_active;

        if (nameButton == "button_main")
            MainScript.ms.fon.transform.Find("button_main").gameObject.GetComponent<Image>().sprite = MainScript.ms.button1_active;

        if (nameButton == "button_heroes")
            MainScript.ms.fon.transform.Find("button_heroes").gameObject.GetComponent<Image>().sprite = MainScript.ms.button23_active;

        if (nameButton == "button_invent")
            MainScript.ms.fon.transform.Find("button_invent").gameObject.GetComponent<Image>().sprite = MainScript.ms.button23_active;




    }

    public void OnPointerClick(PointerEventData eventData)

    {
        int i = 0;





        switch (gameObject.name)
        {
            case "button_back":

                if (MainScript.ms.currentWindow == "fon_select_but1")
                {
                    MainScript.ms.fon_select_but1.SetActive(false);
                    MainScript.ms.fon_main.SetActive(true);
                    StartCoroutine(MainScript.ms.UpdateInfo());

                    MainScript.ms.Fon_main_update();
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_back.SetActive(false);

                    changeColorButtonMain("button_main");


                }
                if (MainScript.ms.currentWindow == "fon_select_but2")
                {
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.fon_main.SetActive(true);
                    StartCoroutine(MainScript.ms.UpdateInfo());
                    MainScript.ms.Fon_main_update();
                    MainScript.ms.UpdateHero();

                    MainScript.ms.button_back.SetActive(false);

                    changeColorButtonMain("button_main");


                }
                if (MainScript.ms.currentWindow == "select_com_level")
                {
                    MainScript.ms.fon_com_level.SetActive(false);
                    MainScript.ms.fon_select_but1.SetActive(true);
                    MainScript.ms.currentWindow = "fon_select_but1";
                }
                if (MainScript.ms.currentWindow == "select_com_weekly")
                {
                    MainScript.ms.fon_weekly.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(false);
                    MainScript.ms.fon_select_but1.SetActive(true);
                    MainScript.ms.currentWindow = "fon_select_but1";

                }

                if (MainScript.ms.currentWindow == "noPVP")
                {
                    MainScript.ms.fon_fight_prepare.SetActive(false);
                    MainScript.ms.fon_select_but2.SetActive(true);
                    MainScript.ms.currentWindow = "fon_select_but2";

                }
                if (MainScript.ms.currentWindow == "PVP")
                {
                    MainScript.ms.fon_fight_prepare.SetActive(false);
                    MainScript.ms.fon_select_but2.SetActive(true);
                    MainScript.ms.currentWindow = "fon_select_but2";

                }

                break;

            case "button_skilltree":

                MainScript.ms.tree.gameObject.SetActive(true);

                break;

            case "CloseTree":
                MainScript.ms.tree.gameObject.SetActive(false);
                break;

            case "button_set":
                MainScript.ms.fon_settings.transform.Find("set1").GetComponentInChildren<Text>().text = MainScript.ms.player.login;
                MainScript.ms.fon_select_but2.SetActive(false);
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.fon_select_but1.SetActive(false);
                MainScript.ms.fon.gameObject.SetActive(true);
                MainScript.ms.fon_main.gameObject.SetActive(true);
                MainScript.ms.fon_settings.gameObject.SetActive(true);
                MainScript.ms.fon_heroes.gameObject.SetActive(false);
                MainScript.ms.fon_shop.gameObject.SetActive(false);
                MainScript.ms.fon_invent.gameObject.SetActive(false);
                MainScript.ms.fon_chat.gameObject.SetActive(false);
                MainScript.ms.Find = false;
                MainScript.ms.fon_com_level.SetActive(false);
                MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);



                break;

            case "ExitAccounts":
                MainScript.ms.exit = true;
                MainScript.ms.fon_login.SetActive(true);
                MainScript.ms.fon_login.transform.Find("menu_login").Find("input_name").GetComponent<InputField>().text = string.Empty;
                MainScript.ms.fon_login.transform.Find("menu_login").Find("input_pass").GetComponent<InputField>().text = string.Empty;
                MainScript.ms.fon_settings.gameObject.SetActive(false);
                break;

            case "button_main":
                MainScript.ms.currentWindow = "";
                MainScript.ms.button_back.SetActive(false);
                MainScript.ms.fon_select_but2.SetActive(false);
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(false);
                MainScript.ms.fon.gameObject.SetActive(true);
                MainScript.ms.fon_main.gameObject.SetActive(true);
                MainScript.ms.fon_settings.gameObject.SetActive(false);
                MainScript.ms.fon_heroes.gameObject.SetActive(false);
                MainScript.ms.fon_shop.gameObject.SetActive(false);
                MainScript.ms.fon_invent.gameObject.SetActive(false);
                MainScript.ms.fon_chat.gameObject.SetActive(false);
                MainScript.ms.Find = false;
                MainScript.ms.fon_select_but1.SetActive(false);

                MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);
                MainScript.ms.Fon_main_update();
                MainScript.ms.UpdateHero();
                StartCoroutine(MainScript.ms.UpdateInfo());

                changeColorButtonMain(gameObject.name);

                break;

            case "button_heroes":
                StartCoroutine(MainScript.ms.FullHP());

                MainScript.ms.button_back.SetActive(false);

                MainScript.ms.fon_select_but2.SetActive(false);
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(false);
                MainScript.ms.fon.gameObject.SetActive(true);
                MainScript.ms.fon_main.gameObject.SetActive(false);
                MainScript.ms.fon_settings.gameObject.SetActive(false);
                MainScript.ms.fon_heroes.gameObject.SetActive(true);
                MainScript.ms.fon_shop.gameObject.SetActive(false);
                MainScript.ms.fon_invent.gameObject.SetActive(false);
                MainScript.ms.Find = false;
                MainScript.ms.fon_select_but1.SetActive(false);
                MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);

                MainScript.ms.UpdateHero();
                changeColorButtonMain(gameObject.name);

                break;

            case "button_shop":
                MainScript.ms.button_back.SetActive(false);

                MainScript.ms.fon_select_but2.SetActive(false);
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(false);
                MainScript.ms.fon.gameObject.SetActive(true);
                MainScript.ms.fon_main.gameObject.SetActive(false);
                MainScript.ms.fon_settings.gameObject.SetActive(false);
                MainScript.ms.fon_heroes.gameObject.SetActive(false);
                MainScript.ms.fon_shop.gameObject.SetActive(true);
                MainScript.ms.fon_invent.gameObject.SetActive(false);
                MainScript.ms.fon_chat.gameObject.SetActive(false);
                MainScript.ms.Find = false;
                MainScript.ms.fon_select_but1.SetActive(false);
                MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);
                MainScript.ms.UpdateShop();

                changeColorButtonMain(gameObject.name);


                break;

            case "button_invent":
                MainScript.ms.button_back.SetActive(false);

                MainScript.ms.fon_select_but2.SetActive(false);
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(false);
                MainScript.ms.fon.gameObject.SetActive(true);
                MainScript.ms.fon_main.gameObject.SetActive(false);
                MainScript.ms.fon_settings.gameObject.SetActive(false);
                MainScript.ms.fon_heroes.gameObject.SetActive(false);
                MainScript.ms.fon_shop.gameObject.SetActive(false);
                MainScript.ms.fon_invent.gameObject.SetActive(true);
                MainScript.ms.Find = false;
                MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);
                MainScript.ms.fon_select_but1.SetActive(false);
                MainScript.ms.fon_chat.gameObject.SetActive(false);

                if (MainScript.ms.player.data.count_sheets >= 10)
                {
                    MainScript.ms.player.data.count_sheets -= 10;
                    MainScript.ms.player.data.count_books++;
                }

                GameObject.Find("CountBooks").GetComponent<Text>().text = "Имеется: " + MainScript.ms.player.data.count_books + " шт.";
                GameObject.Find("CountLists ").GetComponent<Text>().text = MainScript.ms.player.data.count_sheets + "/10";
                changeColorButtonMain(gameObject.name);

                break;
            case "button_main_energy_buy":

                if (MainScript.ms.player.data.gold > 0)
                {

                    MainScript.ms.player.data.gold -= 1;
                    MainScript.ms.player.data.energy += 1;
                    MainScript.ms.Fon_main_update();
                }
                else MainScript.ms.ShowMessage("Ох, батюшки!", "У Вас недостаточно золотишка!\nЗаработой и возвращайся.", "Пошел работать...");

                break;





            case "button_set_exit":
                MainScript.ms.fon_settings.gameObject.SetActive(false);
                break;

            case "button_fight_finder33":
                MainScript.ms.ShowMessage("Упс", "У Вас нет 3х Героев!", "Эх =(");
                break;

            case "button_fight_finder_bot":
                MainScript.ms.ShowMessage("Упс", "Боты пока недоступны", "Эх =(");
                break;

            case "button_fight_finder_history":
                MainScript.ms.ShowMessage("Упс", "Мал ты еще для истории!", "Эх =(");
                break;



            case "button_fight_exit_zabrat":

                StartCoroutine(ScriptLoad.sl.UpdateInfo());

                if (!MainScript.ms.enemy.isbot)
                {
                    WWW www123 = new WWW("");
                    if (MainScript.ms.raiting) www123 = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=true" + "&status=closed");
                    else www123 = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=false" + "&status=closed");
                    while (!www123.isDone) ;

                }

                if (MainScript.ms.currentWindow == "select_com_weekly")
                {
                    MainScript.ms.fon_weekly.SetActive(true);
                    int opa1 = 0;
                    foreach (bool n in MainScript.ms.player.data.weekly_win)
                    {
                        if (n)
                        {
                            MainScript.ms.fon_weekly.transform.GetChild(opa1).GetChild(2).GetChild(1).GetComponent<Text>().text = "ПОЛУЧЕНЫ";
                            MainScript.ms.fon_weekly.transform.GetChild(opa1).GetChild(2).GetChild(1).GetComponent<Text>().color = new Color(0, 1, 0);
                        }
                        else
                        {
                            MainScript.ms.fon_weekly.transform.GetChild(opa1).GetChild(2).GetChild(1).GetComponent<Text>().text = "НЕ ПОЛУЧЕНЫ";
                            MainScript.ms.fon_weekly.transform.GetChild(opa1).GetChild(2).GetChild(1).GetComponent<Text>().color = new Color(1, 0, 0);
                        }
                        opa1++;
                    }
                }

                if (MainScript.ms.currentWindow == "noPVP")
                {
                    MainScript.ms.fon_fight_prepare.transform.GetChild(0).GetComponentInChildren<Text>().text = "БОЙ 3X3. ОБЫЧНЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";
                    MainScript.ms.GainMoney = true;
                    MainScript.ms.raiting = false;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                if (MainScript.ms.currentWindow == "PVP")
                {
                    MainScript.ms.fon_fight_prepare.transform.GetChild(0).GetComponentInChildren<Text>().text = "БОЙ 3X3. РЕЙТИНГОВЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";

                    MainScript.ms.GainMoney = false;
                    MainScript.ms.raiting = true;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                MainScript.ms.fon_fight_res.gameObject.SetActive(false);
                MainScript.ms.fon_fight.gameObject.SetActive(false);
                if (MainScript.ms.ComFight)
                {
                    MainScript.ms.fon_com_level.SetActive(true);
                }


                break;


            case "button_fight_exit_lose_zabrat":
                StartCoroutine(ScriptLoad.sl.UpdateInfo());
                if (MainScript.ms.currentWindow == "noPVP")
                {
                    MainScript.ms.fon_fight_prepare.transform.GetChild(0).GetComponentInChildren<Text>().text = "БОЙ 3X3. ОБЫЧНЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";
                    MainScript.ms.GainMoney = true;
                    MainScript.ms.raiting = false;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                if (MainScript.ms.currentWindow == "PVP")
                {
                    MainScript.ms.fon_fight_prepare.transform.GetChild(0).GetComponentInChildren<Text>().text = "БОЙ 3X3. РЕЙТИНГОВЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";

                    MainScript.ms.GainMoney = false;
                    MainScript.ms.raiting = true;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                MainScript.ms.fon_fight_res_lose.gameObject.SetActive(false);
                MainScript.ms.fon_fight.gameObject.SetActive(false);
                if (MainScript.ms.ComFight)
                {
                    MainScript.ms.fon_com1.transform.Find("ImagesDie").gameObject.SetActive(true);
                    MainScript.ms.fon_com1.transform.Find("ImagesDie").Find("com" + MainScript.ms.CurrentCom + "_dead").GetComponent<Image>().color = new Color(1, 1, 1, 1);
                }

                break;
            case "com1_dead":
            case "com2_dead":
            case "com3_dead":
            case "com4_dead":
            case "com5_dead":
                MainScript.ms.fon_com1.transform.Find("ImagesDie").gameObject.SetActive(false);
                MainScript.ms.fon_com1.transform.Find("ImagesDie").Find("com" + MainScript.ms.CurrentCom + "_dead").GetComponent<Image>().color = new Color(1, 1, 1, 0);
                MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1] = 0;
                MainScript.ms.fon_com1.SetActive(false);
                MainScript.ms.fon_main.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(true);
                break;



            case "her_for_bit":



                if (GameObject.Find("slider_cankick_player1").GetComponent<Slider>().value > 10)
                {

                    MainScript.ms.cankick_slider -= 20;

                    bool crit = false;
                    bool dodge = false;
                    int damage = MainScript.ms.Get_player_alive_hero().damage;
                    //if (UnityEngine.Random.Range(0, 100) < (MainScript.ms.Get_player_alive_hero().crit_persent * MainScript.ms.skill_correction.crit))
                    if (UnityEngine.Random.Range(0, 100) < 20)
                    {
                        damage *= 2;
                        crit = true;
                    }

                    damage = Convert.ToInt32(damage * MainScript.ms.skill_correction.damage);

                    //if (UnityEngine.Random.Range(0, 100) < MainScript.ms.Get_enemy_alive_hero().dodge_persent * MainScript.ms.skill_correction.dodge)
                    if (UnityEngine.Random.Range(0, 100) <20)
                    {
                        damage = 0;
                        crit = false;
                        dodge = true;
                    }


                    MainScript.ms.Send_action("kick", damage + "");
                    var a = Instantiate(MainScript.ms.TextDamagepref, GameObject.Find("fon_fight").transform);
                    if (!dodge)
                    {
                        a.GetComponent<Text>().text = damage.ToString();

                    }
                    else
                    {
                        a.GetComponent<Text>().text = "Промах";
                    }

                    if (crit)
                    {
                        a.GetComponent<Text>().color = new Color(1, 0, 0);
                    }

                    StartCoroutine(Animate_action());



                }




                break;


            case "fight_her1":

                PlayerTapSkill(0);
                break;

            case "fight_her2":

                PlayerTapSkill(1);
                break;

            case "fight_her3":

                PlayerTapSkill(2);
                break;



            case "button_botfight":




                StartCoroutine(MainScript.ms.StartFight());



                //MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot1").GetComponentInChildren<Hero_script>().hero);
                // MainScript.ms.player.data.selected_heroes[1] = MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot2").GetComponentInChildren<Hero_script>().hero);
                // MainScript.ms.player.data.selected_heroes[2] = MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot3").GetComponentInChildren<Hero_script>().hero);



                Player bot = new Player();
                bot.data = new player_data();
                bot.data.lvl = 1;

                bot.login = "Бот";
                bot.isbot = true;

                bot.data.Heros = new List<Hero>();

                bot.data.Heros.Add(new Hero
                {
                    name = "Рэй1",
                    type = UnityEngine.Random.Range(0, 9) + "",
                    lvl = 1,
                    heals = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[0].ToString())].heals - 100,
                    damage = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[0].ToString())].damage * 2,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[0].ToString())].maxHp,
                    damageAll = 0
                });


                bot.data.Heros.Add(new Hero
                {
                    name = "Рэй2",
                    type = UnityEngine.Random.Range(0, 9) + "",
                    lvl = 1,
                    heals = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[1].ToString())].heals - 100,
                    damage = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[1].ToString())].damage * 2,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[1].ToString())].maxHp,
                    damageAll = 0
                });


                bot.data.Heros.Add(new Hero
                {
                    name = "Рэй3",
                    type = UnityEngine.Random.Range(0, 9) + "",
                    lvl = 1,
                    heals = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[2].ToString())].heals - 100,
                    damage = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[2].ToString())].damage * 2,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = MainScript.ms.player.data.Heros[MainScript.ms.player.data.Heros.FindIndex(x => x.type == MainScript.ms.player.data.selected_heroes[2].ToString())].maxHp,
                    damageAll = 0
                });

                //                bot.data.Heros.Add(MainScript.ms.player.data.Heros[MainScript.ms.player.data.selected_heroes[1]]);

                do
                {
                    bot.data.Heros[0].type = UnityEngine.Random.Range(0, 9) + "";
                    bot.data.Heros[1].type = UnityEngine.Random.Range(0, 9) + "";
                    bot.data.Heros[2].type = UnityEngine.Random.Range(0, 9) + "";
                } while ((bot.data.Heros[0].type == bot.data.Heros[1].type) || (bot.data.Heros[1].type == bot.data.Heros[2].type) || (bot.data.Heros[0].type == bot.data.Heros[2].type));



                MainScript.ms.enemy = bot;

                MainScript.ms.enemy.data.selected_heroes[0] = 0;
                MainScript.ms.enemy.data.selected_heroes[1] = 1;
                MainScript.ms.enemy.data.selected_heroes[2] = 2;


                MainScript.ms.internal_fight = new Fight();
                MainScript.ms.internal_fight.players = new List<Player>();
                MainScript.ms.internal_fight.actions_list = new List<Action>();

                MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);

                MainScript.ms.fon_fight_finder.gameObject.SetActive(false);
                MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);

                MainScript.ms.Begin_fight();


                break;

            case "PauseButton":
                MainScript.ms.pause = true;

                MainScript.ms.fon_fight_exit.gameObject.SetActive(true);
                Time.timeScale = 0;
                break;

            case "button_buy":

                break;

            case "button_com_fight":

                if (MainScript.ms.CurrentCom == 1 && MainScript.ms.player.data.comicsPos[0] == 7)
                {

                    MainScript.ms.ComFight = true;
                    bot = new Player();
                    bot.data = new player_data();
                    bot.data.lvl = 1;

                    bot.login = "Демон";
                    bot.isbot = true;

                    bot.data.Heros = new List<Hero>();
                    bot.data.Heros.Add(new Hero
                    {
                        name = "Демон",
                        type = "7",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[0],
                        damage = MainScript.ms.damage_in_lvl[0],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[0],
                        damageAll = 0
                    });


                    MainScript.ms.enemy = bot;

                    MainScript.ms.enemy.data.selected_heroes[0] = 0;
                    MainScript.ms.enemy.data.selected_heroes[1] = -1;
                    MainScript.ms.enemy.data.selected_heroes[2] = -1;

                    MainScript.ms.player.data.selected_heroes[0] = 2;
                    MainScript.ms.player.data.selected_heroes[1] = -1;
                    MainScript.ms.player.data.selected_heroes[2] = -1;

                    MainScript.ms.internal_fight = new Fight();
                    MainScript.ms.internal_fight.players = new List<Player>();
                    MainScript.ms.internal_fight.actions_list = new List<Action>();

                    MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                    MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);

                    MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back01");
                    MainScript.ms.Begin_fight();




                }
                if (MainScript.ms.CurrentCom == 2 && MainScript.ms.player.data.comicsPos[1] == 11)
                {
                    MainScript.ms.ComFight = true;

                    bot = new Player();
                    bot.data = new player_data();
                    bot.data.lvl = 1;

                    bot.login = "Ноил и Демон";
                    bot.isbot = true;

                    bot.data.Heros = new List<Hero>();
                    bot.data.Heros.Add(new Hero
                    {
                        name = "Ноил",
                        type = "1",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[1],
                        damage = MainScript.ms.damage_in_lvl[1],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[1],
                        damageAll = 0
                    });

                    bot.data.Heros.Add(new Hero
                    {
                        name = "Демон",
                        type = "7",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[0],
                        damage = MainScript.ms.damage_in_lvl[0],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[0],
                        damageAll = 0
                    });


                    MainScript.ms.enemy = bot;

                    MainScript.ms.enemy.data.selected_heroes[0] = 0;
                    MainScript.ms.enemy.data.selected_heroes[1] = 1;
                    MainScript.ms.enemy.data.selected_heroes[2] = -1;

                    MainScript.ms.player.data.selected_heroes[0] = 2;
                    MainScript.ms.player.data.selected_heroes[1] = -1;
                    MainScript.ms.player.data.selected_heroes[2] = -1;

                    MainScript.ms.internal_fight = new Fight();
                    MainScript.ms.internal_fight.players = new List<Player>();
                    MainScript.ms.internal_fight.actions_list = new List<Action>();

                    MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                    MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);

                    MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back02");

                    MainScript.ms.Begin_fight();
                }



                if (MainScript.ms.CurrentCom == 3 && MainScript.ms.player.data.comicsPos[2] == 16)
                {
                    MainScript.ms.ComFight = true;

                    bot = new Player();
                    bot.data = new player_data();
                    bot.data.lvl = 1;

                    bot.login = "Водные элементали";
                    bot.isbot = true;

                    bot.data.Heros = new List<Hero>();
                    bot.data.Heros.Add(new Hero
                    {
                        name = "Водный",
                        type = "9",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[2],
                        damage = MainScript.ms.damage_in_lvl[2],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[2],
                        damageAll = 0
                    });


                    MainScript.ms.enemy = bot;

                    MainScript.ms.enemy.data.selected_heroes[0] = 0;
                    MainScript.ms.enemy.data.selected_heroes[1] = -1;
                    MainScript.ms.enemy.data.selected_heroes[2] = -1;

                    MainScript.ms.player.data.selected_heroes[0] = 2;
                    MainScript.ms.player.data.selected_heroes[1] = -1;
                    MainScript.ms.player.data.selected_heroes[2] = -1;

                    MainScript.ms.internal_fight = new Fight();
                    MainScript.ms.internal_fight.players = new List<Player>();
                    MainScript.ms.internal_fight.actions_list = new List<Action>();

                    MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                    MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);
                    MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back03");


                    MainScript.ms.Begin_fight();


                }
                if (MainScript.ms.CurrentCom == 4 && MainScript.ms.player.data.comicsPos[3] == 6)
                {
                    MainScript.ms.ComFight = true;

                    bot = new Player();
                    bot.data = new player_data();
                    bot.data.lvl = 1;

                    bot.login = "Водные элементали";
                    bot.isbot = true;

                    bot.data.Heros = new List<Hero>();
                    bot.data.Heros.Add(new Hero
                    {
                        name = "Водный",
                        type = "9",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[3],
                        damage = MainScript.ms.damage_in_lvl[3],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[3],
                        damageAll = 0
                    });

                    bot.data.Heros.Add(new Hero
                    {
                        name = "Водный",
                        type = "9",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[3],
                        damage = MainScript.ms.damage_in_lvl[3],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[3],
                        damageAll = 0
                    });

                    MainScript.ms.enemy = bot;

                    MainScript.ms.enemy.data.selected_heroes[0] = 0;
                    MainScript.ms.enemy.data.selected_heroes[1] = 1;
                    MainScript.ms.enemy.data.selected_heroes[2] = -1;

                    MainScript.ms.player.data.selected_heroes[0] = 2;
                    MainScript.ms.player.data.selected_heroes[1] = 6;
                    MainScript.ms.player.data.selected_heroes[2] = -1;

                    MainScript.ms.internal_fight = new Fight();
                    MainScript.ms.internal_fight.players = new List<Player>();
                    MainScript.ms.internal_fight.actions_list = new List<Action>();

                    MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                    MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);
                    MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back03");


                    MainScript.ms.Begin_fight();

                }

                if (MainScript.ms.CurrentCom == 5 && MainScript.ms.player.data.comicsPos[4] == 11)
                {
                    MainScript.ms.ComFight = true;

                    bot = new Player();
                    bot.data = new player_data();
                    bot.data.lvl = 1;

                    bot.login = "Деревянными элементали";
                    bot.isbot = true;

                    bot.data.Heros = new List<Hero>();
                    bot.data.Heros.Add(new Hero
                    {
                        name = "Деревянными",
                        type = "5",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[4],
                        damage = MainScript.ms.damage_in_lvl[4],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[4],
                        damageAll = 0
                    });
                    bot.data.Heros.Add(new Hero
                    {
                        name = "Деревянными",
                        type = "5",
                        lvl = 1,
                        heals = MainScript.ms.hp_in_lvl[4],
                        damage = MainScript.ms.damage_in_lvl[4],
                        crit_persent = 0.5f,
                        dodge_persent = 0.5f,
                        maxHp = MainScript.ms.hp_in_lvl[4],
                        damageAll = 0
                    });

                    MainScript.ms.enemy = bot;

                    MainScript.ms.enemy.data.selected_heroes[0] = 0;
                    MainScript.ms.enemy.data.selected_heroes[1] = 1;
                    MainScript.ms.enemy.data.selected_heroes[2] = -1;

                    MainScript.ms.player.data.selected_heroes[0] = 2;
                    MainScript.ms.player.data.selected_heroes[1] = 6;
                    MainScript.ms.player.data.selected_heroes[2] = -1;

                    MainScript.ms.internal_fight = new Fight();
                    MainScript.ms.internal_fight.players = new List<Player>();
                    MainScript.ms.internal_fight.actions_list = new List<Action>();

                    MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                    MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);
                    MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back03");


                    MainScript.ms.Begin_fight();

                }

                break;

            case "button_com_next_page":
                HideAllCom();
                MainScript.ms.fon_com1.transform.Find("button_com_back_page").gameObject.SetActive(true);
                MainScript.ms.fon_com1.transform.Find("button_com_next_page").gameObject.SetActive(false);
                for (int a = 8; a <= MainScript.ms.player.data.comicsPos[3]; a++)
                {
                    var gg = GameObject.Find("com4_" + a);
                    gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                }

                break;

            case "button_com_back_page":
                HideAllCom();
                MainScript.ms.fon_com1.transform.Find("button_com_back_page").gameObject.SetActive(false);
                MainScript.ms.fon_com1.transform.Find("button_com_next_page").gameObject.SetActive(true);
                for (int a = 1; a < 8; a++)
                {
                    var gg = GameObject.Find("com4_" + a);
                    gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                }

                break;

            case "fon_com1_tap":

                if (MainScript.ms.CurrentCom == 1 && MainScript.ms.player.data.comicsPos[0] < 15)
                {
                    if (MainScript.ms.player.data.energy > 0)
                    {
                        if ((MainScript.ms.player.data.comiscKillBot[0] && MainScript.ms.player.data.comicsPos[0] == 7) || MainScript.ms.player.data.comicsPos[0] != 7)
                        {
                            MainScript.ms.player.data.energy--;
                            MainScript.ms.player.data.comicsPos[0]++;


                            GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                            GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                            StartCoroutine(curcom());
                            if (MainScript.ms.player.data.comicsPos[0] == 7)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                            }
                            if (MainScript.ms.player.data.comicsPos[0] == 15)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                            }
                        }

                    }
                }
                if (MainScript.ms.CurrentCom == 2 && MainScript.ms.player.data.comicsPos[1] < 18)
                {
                    if (MainScript.ms.player.data.energy > 0)
                    {
                        if ((MainScript.ms.player.data.comiscKillBot[1] && MainScript.ms.player.data.comicsPos[1] == 11) || MainScript.ms.player.data.comicsPos[1] != 11)
                        {
                            MainScript.ms.player.data.energy--;
                            MainScript.ms.player.data.comicsPos[1]++;


                            GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                            GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                            StartCoroutine(curcom());
                            if (MainScript.ms.player.data.comicsPos[1] == 11)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                            }
                            if (MainScript.ms.player.data.comicsPos[1] == 18)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                            }
                        }

                    }
                }

                if (MainScript.ms.CurrentCom == 3 && MainScript.ms.player.data.comicsPos[2] < 18)
                {
                    if (MainScript.ms.player.data.energy > 0)
                    {
                        if ((MainScript.ms.player.data.comiscKillBot[2] && MainScript.ms.player.data.comicsPos[2] == 16) || MainScript.ms.player.data.comicsPos[2] != 16)
                        {
                            MainScript.ms.player.data.energy--;
                            MainScript.ms.player.data.comicsPos[2]++;


                            GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                            GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                            StartCoroutine(curcom());
                            if (MainScript.ms.player.data.comicsPos[2] == 16)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                            }
                            if (MainScript.ms.player.data.comicsPos[2] == 18)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                            }
                        }

                    }
                }

                if (MainScript.ms.CurrentCom == 4 && MainScript.ms.player.data.comicsPos[3] < 16)
                {
                    if (MainScript.ms.player.data.energy > 0)
                    {
                        if ((MainScript.ms.player.data.comiscKillBot[3] && MainScript.ms.player.data.comicsPos[3] == 6) || MainScript.ms.player.data.comicsPos[3] != 6)
                        {
                            if (!MainScript.ms.fon_com1.transform.Find("button_com_next_page").gameObject.activeSelf)
                            {
                                MainScript.ms.player.data.energy--;
                                MainScript.ms.player.data.comicsPos[3]++;


                                GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                                GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                                StartCoroutine(curcom());
                                if (MainScript.ms.player.data.comicsPos[3] == 6)
                                {
                                    MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                    MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                                }
                                if (MainScript.ms.player.data.comicsPos[3] == 16)
                                {
                                    MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                                    MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                                    MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                    MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                                }
                            }
                        }

                    }
                }


                if (MainScript.ms.CurrentCom == 5 && MainScript.ms.player.data.comicsPos[4] < 14)
                {
                    if (MainScript.ms.player.data.energy > 0)
                    {
                        if ((MainScript.ms.player.data.comiscKillBot[4] && MainScript.ms.player.data.comicsPos[4] == 11) || MainScript.ms.player.data.comicsPos[4] != 11)
                        {
                            MainScript.ms.player.data.energy--;
                            MainScript.ms.player.data.comicsPos[4]++;


                            GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                            GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                            StartCoroutine(curcom());
                            if (MainScript.ms.player.data.comicsPos[4] == 11)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                            }
                            if (MainScript.ms.player.data.comicsPos[4] == 14)
                            {
                                MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                                MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                            }
                        }

                    }
                }

                //if (MainScript.ms.player.data.comix_position_index == 12) for (int i1 = 1; i1 <= 12; i1++) GameObject.Find("com1_" + i1).GetComponent<Image>().color = new Vector4(1, 1, 1, 0);

                //if (MainScript.ms.player.data.comix_position_index == 18) for (int i1 = 1; i1 <= 18; i1++) GameObject.Find("com1_" + i1).GetComponent<Image>().color = new Vector4(1, 1, 1, 0);
                //if (MainScript.ms.player.data.comix_position_index == 24) for (int i1 = 1; i1 <= 24; i1++) GameObject.Find("com1_" + i1).GetComponent<Image>().color = new Vector4(1, 1, 1, 0);
                //if (MainScript.ms.player.data.comix_position_index == 27) for (int i1 = 1; i1 <= 27; i1++) GameObject.Find("com1_" + i1).GetComponent<Image>().color = new Vector4(1, 1, 1, 0);

                //if (MainScript.ms.player.data.comix_position_index < 37)
                //{


                //    if (MainScript.ms.player.data.energy > 0)
                //    {
                //        MainScript.ms.player.data.energy--;
                //        MainScript.ms.player.data.comix_position_index++;


                //        GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                //        GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;




                //        StartCoroutine(curcom());




                //        if (MainScript.ms.player.data.comix_position_index == 6)
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Бой с демоном";


                //        if (MainScript.ms.player.data.comix_position_index == 7)
                //        {
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Коснитесь экрана для продолжения";


                //            if (MainScript.ms.player.data.Heros.Find(x => x.type == "2") == null)
                //                MainScript.ms.player.data.Heros.Add(new Hero
                //                {
                //                    name = "Рэй",
                //                    type = "2",
                //                    lvl = 1,
                //                    heals = 400,
                //                    damage = 20,
                //                    crit_persent = 0.5f,
                //                    dodge_persent = 0.5f,
                //                    maxHp = 400,
                //                    damageAll = 0
                //                });


                //            bot = new Player();
                //            bot.data = new player_data();
                //            bot.data.lvl = 1;

                //            bot.login = "Демон";
                //            bot.isbot = true;

                //            bot.data.Heros = new List<Hero>();
                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Демон",
                //                type = "7",
                //                lvl = 1,
                //                heals = 100,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });


                //            MainScript.ms.enemy = bot;

                //            MainScript.ms.enemy.data.selected_heroes[0] = 0;
                //            MainScript.ms.enemy.data.selected_heroes[1] = -1;
                //            MainScript.ms.enemy.data.selected_heroes[2] = -1;

                //            MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == "2"));
                //            MainScript.ms.player.data.selected_heroes[1] = -1;
                //            MainScript.ms.player.data.selected_heroes[2] = -1;

                //            MainScript.ms.internal_fight = new Fight();
                //            MainScript.ms.internal_fight.players = new List<Player>();
                //            MainScript.ms.internal_fight.actions_list = new List<Action>();

                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);

                //            MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back01");
                //            MainScript.ms.Begin_fight();

                //        }

                //        if (MainScript.ms.player.data.comix_position_index == 14)
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Бой с Ноилом и Демоном";


                //        if (MainScript.ms.player.data.comix_position_index == 15)
                //        {
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Коснитесь экрана для продолжения";

                //            Debug.Log("Begin fight");


                //            bot = new Player();
                //            bot.data = new player_data();
                //            bot.data.lvl = 1;

                //            bot.login = "Ноил и Демон";
                //            bot.isbot = true;

                //            bot.data.Heros = new List<Hero>();
                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Ноил",
                //                type = "1",
                //                lvl = 1,
                //                heals = 200,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });

                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Демон",
                //                type = "7",
                //                lvl = 1,
                //                heals = 100,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });


                //            MainScript.ms.enemy = bot;

                //            MainScript.ms.enemy.data.selected_heroes[0] = 0;
                //            MainScript.ms.enemy.data.selected_heroes[1] = 1;
                //            MainScript.ms.enemy.data.selected_heroes[2] = -1;

                //            MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == "2"));
                //            MainScript.ms.player.data.selected_heroes[1] = -1;
                //            MainScript.ms.player.data.selected_heroes[2] = -1;

                //            MainScript.ms.internal_fight = new Fight();
                //            MainScript.ms.internal_fight.players = new List<Player>();
                //            MainScript.ms.internal_fight.actions_list = new List<Action>();

                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);

                //            MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back02");

                //            MainScript.ms.Begin_fight();

                //        }




                //        if (MainScript.ms.player.data.comix_position_index == 22)
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Бой с Водными элементалями";


                //        if (MainScript.ms.player.data.comix_position_index == 23)
                //        {
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Коснитесь экрана для продолжения";



                //            bot = new Player();
                //            bot.data = new player_data();
                //            bot.data.lvl = 1;

                //            bot.login = "Водные элементали";
                //            bot.isbot = true;

                //            bot.data.Heros = new List<Hero>();
                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Водный",
                //                type = "9",
                //                lvl = 1,
                //                heals = 400,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });


                //            MainScript.ms.enemy = bot;

                //            MainScript.ms.enemy.data.selected_heroes[0] = 0;
                //            MainScript.ms.enemy.data.selected_heroes[1] = -1;
                //            MainScript.ms.enemy.data.selected_heroes[2] = -1;

                //            MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == "2"));
                //            MainScript.ms.player.data.selected_heroes[1] = -1;
                //            MainScript.ms.player.data.selected_heroes[2] = -1;

                //            MainScript.ms.internal_fight = new Fight();
                //            MainScript.ms.internal_fight.players = new List<Player>();
                //            MainScript.ms.internal_fight.actions_list = new List<Action>();

                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);
                //            MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back03");


                //            MainScript.ms.Begin_fight();

                //        }






                //        if (MainScript.ms.player.data.comix_position_index == 25)
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Бой с Водными элементалями";


                //        if (MainScript.ms.player.data.comix_position_index == 26)
                //        {
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Коснитесь экрана для продолжения";



                //            bot = new Player();
                //            bot.data = new player_data();
                //            bot.data.lvl = 1;

                //            bot.login = "Водные элементали";
                //            bot.isbot = true;

                //            bot.data.Heros = new List<Hero>();
                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Водный",
                //                type = "9",
                //                lvl = 1,
                //                heals = 400,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });

                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Водный",
                //                type = "9",
                //                lvl = 1,
                //                heals = 400,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });


                //            MainScript.ms.enemy = bot;

                //            MainScript.ms.enemy.data.selected_heroes[0] = 0;
                //            MainScript.ms.enemy.data.selected_heroes[1] = 1;
                //            MainScript.ms.enemy.data.selected_heroes[2] = -1;

                //            MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == "2"));
                //            MainScript.ms.player.data.selected_heroes[1] = -1;
                //            MainScript.ms.player.data.selected_heroes[2] = -1;

                //            MainScript.ms.internal_fight = new Fight();
                //            MainScript.ms.internal_fight.players = new List<Player>();
                //            MainScript.ms.internal_fight.actions_list = new List<Action>();

                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);


                //            MainScript.ms.Begin_fight();

                //        }








                //        if (MainScript.ms.player.data.comix_position_index == 34)
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Бой с Деревянными элементалями";


                //        if (MainScript.ms.player.data.comix_position_index == 35)
                //        {
                //            GameObject.Find("fon_com1_taptext").GetComponent<Text>().text = "Коснитесь экрана для продолжения";


                //            bot = new Player();
                //            bot.data = new player_data();
                //            bot.data.lvl = 1;

                //            bot.login = "Деревянными элементали";
                //            bot.isbot = true;

                //            bot.data.Heros = new List<Hero>();
                //            bot.data.Heros.Add(new Hero
                //            {
                //                name = "Деревянными",
                //                type = "5",
                //                lvl = 1,
                //                heals = 400,
                //                damage = 20,
                //                crit_persent = 0.5f,
                //                dodge_persent = 0.5f,
                //                maxHp = 400,
                //                damageAll = 0
                //            });


                //            MainScript.ms.enemy = bot;

                //            MainScript.ms.enemy.data.selected_heroes[0] = 0;
                //            MainScript.ms.enemy.data.selected_heroes[1] = -1;
                //            MainScript.ms.enemy.data.selected_heroes[2] = -1;

                //            MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(MainScript.ms.player.data.Heros.Find(x => x.type == "2"));
                //            MainScript.ms.player.data.selected_heroes[1] = -1;
                //            MainScript.ms.player.data.selected_heroes[2] = -1;

                //            MainScript.ms.internal_fight = new Fight();
                //            MainScript.ms.internal_fight.players = new List<Player>();
                //            MainScript.ms.internal_fight.actions_list = new List<Action>();

                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
                //            MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);
                //            MainScript.ms.fon_fight.GetComponent<Image>().sprite = Resources.Load<Sprite>("back04");

                //            MainScript.ms.Begin_fight();

                //        }






                //    }
                //    else
                //        MainScript.ms.ShowMessage("Не хватает маны!", "Зарядись и продолжи свой путь!", "Ок, пошел посплю!");
                //}

                break;







            case "button_fight_exit":
            case "button_fight_pause":

                MainScript.ms.fon_fight_exit.gameObject.SetActive(true);

                break;

            case "button_fight_exit_conf":
                StartCoroutine(MainScript.ms.UpdateInfo());

                if (MainScript.ms.currentWindow == "select_com_weekly")
                {
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                    MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().color = new Color(1, 1, 1);

                }
                if (MainScript.ms.currentWindow == "noPVP")
                {
                    MainScript.ms.fon_fight_prepare.transform.GetChild(0).GetComponentInChildren<Text>().text = "БОЙ 3X3. ОБЫЧНЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";
                    MainScript.ms.GainMoney = true;
                    MainScript.ms.raiting = false;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                if (MainScript.ms.currentWindow == "PVP")
                {
                    MainScript.ms.fon_fight_prepare.transform.GetChild(0).GetComponentInChildren<Text>().text = "БОЙ 3X3. РЕЙТИНГОВЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";

                    MainScript.ms.GainMoney = false;
                    MainScript.ms.raiting = true;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                MainScript.ms.Send_action("leave", "0");
                MainScript.ms.pause = false;
                MainScript.ms.LastClick = "0";
                MainScript.ms.fon_fight_exit.gameObject.SetActive(false);
                MainScript.ms.fon_fight.gameObject.SetActive(false);
                MainScript.ms.fon.gameObject.SetActive(true);
                if (MainScript.ms.ComFight)
                {
                    MainScript.ms.fon_com1.transform.Find("ImagesDie").gameObject.SetActive(true);
                    MainScript.ms.fon_com1.transform.Find("ImagesDie").Find("com" + MainScript.ms.CurrentCom + "_dead").GetComponent<Image>().color = new Color(1, 1, 1, 1);
                }
                Time.timeScale = 1;



                break;

            case "button_fight_back":

                MainScript.ms.fon_fight_exit.gameObject.SetActive(false);
                MainScript.ms.pause = false;
                Time.timeScale = 1;

                break;



            case "button_fight_bots_back":
                Time.timeScale = 1;
                GameObject.Find("fon_fight_bots_exit").SetActive(false);

                break;








            case "button_chat":
                MainScript.ms.fon_chat.gameObject.SetActive(true);
                break;
            case "text_chat":
                MainScript.ms.fon_chat.gameObject.SetActive(true);
                break;


            case "button_chat_all":

                MainScript.ms.menu_chat_all.gameObject.SetActive(true);
                MainScript.ms.menu_chat_poisk.gameObject.SetActive(false);
                MainScript.ms.menu_chat_friends.gameObject.SetActive(false);
                MainScript.ms.menu_chat_invites.gameObject.SetActive(false);
                MainScript.ms.menu_chat_admin.gameObject.SetActive(false);

                break;


            case "button_chat_poisk":
                MainScript.ms.menu_chat_all.gameObject.SetActive(false);
                MainScript.ms.menu_chat_poisk.gameObject.SetActive(true);
                MainScript.ms.menu_chat_friends.gameObject.SetActive(false);
                MainScript.ms.menu_chat_invites.gameObject.SetActive(false);
                MainScript.ms.menu_chat_admin.gameObject.SetActive(false);
                break;

            case "button_chat_friends":
                MainScript.ms.menu_chat_all.gameObject.SetActive(false);
                MainScript.ms.menu_chat_poisk.gameObject.SetActive(false);
                MainScript.ms.menu_chat_friends.gameObject.SetActive(true);
                MainScript.ms.menu_chat_invites.gameObject.SetActive(false);
                MainScript.ms.menu_chat_admin.gameObject.SetActive(false);

                foreach (Transform a in MainScript.ms.menu_chat_friends.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                {
                    Destroy(a.gameObject);
                }
                foreach (friends n in MainScript.ms.player.data.friends)
                {
                    var asd = Instantiate(MainScript.ms.ItemFind, GameObject.Find("menu_chat_friends").transform.Find("Scroll View").Find("Viewport").Find("Content").transform);
                    asd.GetComponent<FindedPlayer>().id = n.pid;
                    asd.GetComponent<FindedPlayer>().name = n.name;
                    asd.GetComponent<FindedPlayer>().pass = n.pass;
                    asd.name = n.name;
                    asd.transform.Find("buttonAdd").gameObject.SetActive(false);
                    asd.transform.Find("Text").GetComponent<Text>().text = n.name;
                }


                break;

            case "button_chat_invites":
                MainScript.ms.menu_chat_all.gameObject.SetActive(false);
                MainScript.ms.menu_chat_poisk.gameObject.SetActive(false);
                MainScript.ms.menu_chat_friends.gameObject.SetActive(false);
                MainScript.ms.menu_chat_invites.gameObject.SetActive(true);
                MainScript.ms.menu_chat_admin.gameObject.SetActive(false);

                foreach (Transform n in GameObject.Find("menu_chat_invites").transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                {
                    Destroy(n.gameObject);
                }
                foreach (requestFriend friend in MainScript.ms.player.data.requestFriends)
                {
                    var asd = Instantiate(MainScript.ms.ItemFind, GameObject.Find("menu_chat_invites").transform.Find("Scroll View").Find("Viewport").Find("Content").transform);
                    asd.GetComponent<FindedPlayer>().id = friend.pid;
                    asd.GetComponent<FindedPlayer>().name = friend.name;
                    asd.GetComponent<FindedPlayer>().pass = friend.pass;
                    asd.transform.Find("Text").GetComponent<Text>().text = friend.name;
                    asd.name = friend.name;
                }
                foreach (Transform n in MainScript.ms.menu_chat_invites.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                {
                    if (MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text != "")
                    {
                        if (n.name.Contains(MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text))
                        {
                            n.gameObject.SetActive(true);
                        }
                        else
                        {
                            n.gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        n.gameObject.SetActive(true);
                    }
                }

                break;

            case "button_chat_admin":
                MainScript.ms.menu_chat_all.gameObject.SetActive(false);
                MainScript.ms.menu_chat_poisk.gameObject.SetActive(false);
                MainScript.ms.menu_chat_friends.gameObject.SetActive(false);
                MainScript.ms.menu_chat_invites.gameObject.SetActive(false);
                MainScript.ms.menu_chat_admin.gameObject.SetActive(true);
                break;



            case "button_chat_sendall":

                string text = GameObject.Find("input_chat_all").GetComponentInChildren<Text>().text;
                if (text != "")
                {
                    new WWW(MainScript.ms.server_url + "chat.php?user_id=" + MainScript.ms.player.pid + "&to_id=0&text=" + text);
                    Debug.Log(MainScript.ms.server_url + "chat.php?user_id=" + MainScript.ms.player.pid + "&to_id=0&text=" + text);
                    GameObject.Find("input_chat_all").GetComponentInChildren<InputField>().text = "";
                }
                break;


            case "button_login":

                MainScript.ms.exit = false;


                string login = GameObject.Find("input_name").GetComponentInChildren<Text>().text;
                string pass = GameObject.Find("input_pass").GetComponentInChildren<Text>().text;


                WWW www = new WWW(MainScript.ms.server_url + "get_login.php?login=" + login + " &pass=" + pass);
                while (!www.isDone) ;


                MainScript.ms.player = JsonUtility.FromJson<Player>(www.text);



                //if (player.data.LastJoinDate = currentTime.text.Substring(16, 21).Replace("/", ""))
                //{

                //}



                if (GameObject.Find("input_name").GetComponentInChildren<Text>().text.Trim().Length == 0 || GameObject.Find("input_pass").GetComponentInChildren<Text>().text.Trim().Length == 0)
                {
                    MainScript.ms.ShowMessage("Ошибка", "Заполните поля логина и пароля", "Ок, понял!");
                }
                else

             if (MainScript.ms.player.error == 1) MainScript.ms.ShowMessage("Ошибка", "Проверьте правильность логин и пароля и попробуйте вновь ;)", "Ок, проверю!");
                else
                {
                    WWW currentTime = new WWW(MainScript.ms.server_url + "get_data.php");
                    while (!currentTime.isDone) ;

                 

                  
                    if ((Convert.ToDateTime(currentTime.text.Substring(16, 21).Replace("\\", "")) - Convert.ToDateTime(MainScript.ms.player.data.LastJoinDate)).TotalMinutes > 1)
                    {
                        MainScript.ms.player.data.energy += Convert.ToInt32((Convert.ToDateTime(currentTime.text.Substring(16, 21).Replace("\\", "")) - Convert.ToDateTime(MainScript.ms.player.data.LastJoinDate)).TotalMinutes);
                        if (MainScript.ms.player.data.energy > MainScript.ms.player.data.energy_max)
                        {
                            MainScript.ms.player.data.energy = MainScript.ms.player.data.energy_max;
                        }
                    }
                    StartCoroutine(ScriptLoad.sl.loadTimer());
                    MainScript.ms.fon_login.SetActive(false);
                    MainScript.ms.fon_main.SetActive(true);
                    MainScript.ms.Fon_main_update();
                    foreach (Transform n in MainScript.ms.menu_chat_poisk.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                    {
                        Destroy(n.gameObject);
                    }

                }



                break;


            case "button_reg":

                MainScript.ms.exit = false;

                login = GameObject.Find("input_name").GetComponentInChildren<Text>().text;
                pass = GameObject.Find("input_pass").GetComponentInChildren<Text>().text;



                www = new WWW(MainScript.ms.server_url + "get_register.php?login=" + login + "&pass=" + pass);
                while (!www.isDone) ;

                MainScript.ms.player = JsonUtility.FromJson<Player>(www.text);

                if (GameObject.Find("input_name").GetComponentInChildren<Text>().text.Trim().Length == 0 || GameObject.Find("input_pass").GetComponentInChildren<Text>().text.Trim().Length == 0)
                {
                    MainScript.ms.ShowMessage("Ошибка", "Заполните поля логина и пароля", "Ок, понял!");
                }
                else
                if (MainScript.ms.player.error == 1) MainScript.ms.ShowMessage("Ошибка", "Таколь пользователь ужи числиться в комманде", "Ок, понял!");
                else
                {
                    MainScript.ms.player.data.UnlockMoney = 50;

                    StartCoroutine(ScriptLoad.sl.loadTimer());
                    MainScript.ms.fon_login.SetActive(false);
                    MainScript.ms.fon_main.SetActive(true);

                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero1", name = "Ноил", cost = 1 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero2", name = "Рэй", cost = 2 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero3", name = "Маамун", cost = 3 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero4", name = "Айна", cost = 4 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero5", name = "Деревянный", cost = 4 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero6", name = "Лиа", cost = 1 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero7", name = "Демон", cost = 1 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero8", name = "Призрак", cost = 1 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero9", name = "Водный", cost = 1 });
                    MainScript.ms.player.data.shop_items.Add(new Shop_item { art = "hero10", name = "Каменный", cost = 1 });
                    MainScript.ms.Fon_main_update();

                }
                MainScript.ms.player.error = 0;

                break;




            case "button_chat_close":
                MainScript.ms.fon_chat.gameObject.SetActive(false);
                break;

            case "pvp_no_raiting":
                MainScript.ms.currentWindow = "noPVP";
                MainScript.ms.fon_fight_prepare.transform.GetChild(1).GetComponentInChildren<Text>().text = "БОЙ 3X3. ОБЫЧНЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";
                MainScript.ms.GainMoney = true;
                MainScript.ms.raiting = false;
                MainScript.ms.fon_select_but2.SetActive(false);
                MainScript.ms.UpdateHero();
                MainScript.ms.button_botfight.SetActive(false);
                MainScript.ms.fon_fight_prepare.SetActive(true);
                //pvp();
                break;

            case "pvp_raiting":
                if (MainScript.ms.player.data.lvl > 2)
                {
                    MainScript.ms.currentWindow = "PVP";
                    MainScript.ms.fon_fight_prepare.transform.GetChild(1).GetComponentInChildren<Text>().text = "БОЙ 3X3. РЕЙТИНГОВЫЙ \n<size=12> ВЫБОР ГЕРОЕВ </size> ";

                    MainScript.ms.GainMoney = false;
                    MainScript.ms.raiting = true;
                    MainScript.ms.fon_select_but2.SetActive(false);
                    MainScript.ms.UpdateHero();
                    MainScript.ms.button_botfight.SetActive(false);
                    MainScript.ms.fon_fight_prepare.SetActive(true);
                }
                else
                {
                    MainScript.ms.fon_select_but2.transform.GetChild(3).gameObject.SetActive(true);
                }
                // pvp();
                break;

            case "button_message_exit":
                MainScript.ms.fon_message.gameObject.SetActive(false);
                break;

            case "select_button_quest":
                MainScript.ms.UpdateHero();
                MainScript.ms.currentWindow = "select_com_weekly";
                MainScript.ms.fon_fight_prepare.transform.GetChild(1).GetComponentInChildren<Text>().text = "БОЙ 3X3. ЕЖЕНЕДЕЛЬНЫЙ " + Convert.ToChar(13) + "\n <size=12> ВЫБОР ГЕРОЕВ</size> ";
                MainScript.ms.GainMoney = false;
                MainScript.ms.fon_weekly.SetActive(true);
                MainScript.ms.fon_select_but1.SetActive(false);
                int opa = 0;
                foreach(bool n in MainScript.ms.player.data.weekly_win)
                {
                    if (n)
                    {
                        MainScript.ms.fon_weekly.transform.GetChild(opa).GetChild(2).GetChild(1).GetComponent<Text>().text = "ПОЛУЧЕНЫ";
                        MainScript.ms.fon_weekly.transform.GetChild(opa).GetChild(2).GetChild(1).GetComponent<Text>().color = new Color(0,1,0);
                    }
                    else
                    {
                        MainScript.ms.fon_weekly.transform.GetChild(opa).GetChild(2).GetChild(1).GetComponent<Text>().text = "НЕ ПОЛУЧЕНЫ";
                        MainScript.ms.fon_weekly.transform.GetChild(opa).GetChild(2).GetChild(1).GetComponent<Text>().color = new Color(1, 0, 0);
                    }
                    opa++;
                }
                break;

            case "select_button_com":
                MainScript.ms.currentWindow = "select_com_level";
                MainScript.ms.GainMoney = false;
                MainScript.ms.fon_select_but1.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(true);
                int z = 0;
                foreach(bool n in MainScript.ms.player.data.comiscComplete)
                {
                    if (n)
                    {
                        MainScript.ms.buttons_com_levels[z].color = new Color(0.02f, 1, 0.08f, 1);
                    }
                    else
                    {
                        MainScript.ms.buttons_com_levels[z].color = new Color(1, 1, 1, 1);

                    }
                    z++;

                }
                break;


            case "button_main_but1":
                StartCoroutine(MainScript.ms.FullHP());

                MainScript.ms.currentWindow = "fon_select_but1";
                MainScript.ms.fon_com1.transform.Find("confirm_leave").gameObject.SetActive(false);
                MainScript.ms.button_back.SetActive(true);
                MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").gameObject.SetActive(false);
                MainScript.ms.GainMoney = false;
                MainScript.ms.button_botfight.SetActive(false);
                MainScript.ms.ComFight = false;
                MainScript.ms.raiting = false;
                MainScript.ms.fon_select_but1.SetActive(true);
                MainScript.ms.fon_main.SetActive(false);
                //MainScript.ms.fon_com1.SetActive(true);
                changeColorButtonMain("");
                //GameObject.Find("fon_com1_text_rating").GetComponent<Text>(pvp_raiting).text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                //GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
                //GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;




                break;

            case "button_com_reward":
                int z1 = 0;
                MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1] = true;
                // MainScript.ms.fon_com1.SetActive(false);
                //MainScript.ms.fon_com_level.SetActive(true);
                MainScript.ms.fon_end_quest.SetActive(true);
                if (MainScript.ms.CurrentCom == 1)
                {
                    
 MainScript.ms.fon_end_quest.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);

                    foreach(Transform n in MainScript.ms.Content_for_reward_comics.transform)
                    {
                        Destroy(n.gameObject);
                    }
                    MainScript.ms.player.data.gold += 10;
                    MainScript.ms.player.data.count_books += 1;
                    var a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[0];
                    a.GetComponentInChildren<Text>().text = "x10";
                    a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[1];
                    a.GetComponentInChildren<Text>().text = "x1";
                    MainScript.ms.Player_data_save();
                    MainScript.ms.UpdateInfo();
                    GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);
                    StartCoroutine(waiting_button());
                }
                if (MainScript.ms.CurrentCom == 2)
                {
                    MainScript.ms.fon_end_quest.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);

                    foreach (Transform n in MainScript.ms.Content_for_reward_comics.transform)
                    {
                        Destroy(n.gameObject);
                    }
                    MainScript.ms.player.data.gold += 30;
                    MainScript.ms.player.data.count_books += 2;
                    var a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[0];
                    a.GetComponentInChildren<Text>().text = "x30";
                    a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[1];
                    a.GetComponentInChildren<Text>().text = "x2";
                    MainScript.ms.Player_data_save();
                    MainScript.ms.UpdateInfo();
            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);
                    StartCoroutine(waiting_button());
                }
                if (MainScript.ms.CurrentCom == 3)
                {
                    MainScript.ms.fon_end_quest.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);

                    foreach (Transform n in MainScript.ms.Content_for_reward_comics.transform)
                    {
                        Destroy(n.gameObject);
                    }
                    MainScript.ms.player.data.gold += 50;
                    MainScript.ms.player.data.count_books += 3;
                    var a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[0];
                    a.GetComponentInChildren<Text>().text = "x50";
                    a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[1];
                    a.GetComponentInChildren<Text>().text = "x3";
                    MainScript.ms.Player_data_save();
                    MainScript.ms.UpdateInfo();
            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);
                    StartCoroutine(waiting_button());
                }
                if (MainScript.ms.CurrentCom == 4)
                {
                    MainScript.ms.fon_end_quest.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);

                    foreach (Transform n in MainScript.ms.Content_for_reward_comics.transform)
                    {
                        Destroy(n.gameObject);
                    }
                    MainScript.ms.player.data.gold += 100;
                    MainScript.ms.player.data.count_books += 4;
                    var a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[0];
                    a.GetComponentInChildren<Text>().text = "x100";
                    a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[1];
                    a.GetComponentInChildren<Text>().text = "x4";
                    MainScript.ms.Player_data_save();
                    MainScript.ms.UpdateInfo();


            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);
                    StartCoroutine(waiting_button());
                }
                if (MainScript.ms.CurrentCom == 5)
                {
                    MainScript.ms.fon_end_quest.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);

                    foreach (Transform n in MainScript.ms.Content_for_reward_comics.transform)
                    {
                        Destroy(n.gameObject);
                    }
                    MainScript.ms.player.data.gold += 120;
                    MainScript.ms.player.data.count_books += 5;
                    var a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[0];
                    a.GetComponentInChildren<Text>().text = "x120";
                    a = Instantiate(MainScript.ms.prefab_reward, MainScript.ms.Content_for_reward_comics.transform);
                    a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[1];
                    a.GetComponentInChildren<Text>().text = "x5";
                    MainScript.ms.Player_data_save();
                    MainScript.ms.UpdateInfo();

            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);
                    StartCoroutine(waiting_button());
                }

                foreach (bool n in MainScript.ms.player.data.comiscComplete)
                {
                    if (n)
                    {
                        MainScript.ms.buttons_com_levels[z1].color = new Color(0.02f, 1, 0.08f, 1);
                    }
                    else
                    {
                        MainScript.ms.buttons_com_levels[z1].color = new Color(1, 1, 1, 1);

                    }
                    z1++;

                }
                break;

            //комиксы
            case "com_icon1":

                if (MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Рэй")))
                {
                    
                    HideAllCom();
                    MainScript.ms.fon_com_level.SetActive(false);
                    MainScript.ms.fon_com1.SetActive(true);
                    GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                    MainScript.ms.CurrentCom = 1;
                    if (MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom-1])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                    }
                    else
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(true);

                    }
                    for (int kk = 1; kk <= MainScript.ms.player.data.comicsPos[0]; kk++)
                    {
                        MainScript.ms.fon_com1.transform.Find("Image").Find("com1_" + kk).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(true);

                    }
                    if (MainScript.ms.player.data.comicsPos[0] == 7 && !MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                    }
                    if (MainScript.ms.player.data.comicsPos[0] == 15 && !MainScript.ms.player.data.comiscComplete[0])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);

                    }
                    if (MainScript.ms.player.data.comicsPos[0] == 15 && MainScript.ms.player.data.comiscComplete[0])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(true);

                    }



                }
                else

                {
                    MainScript.ms.fon_no_hero.SetActive(true);
                    MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМ ГЕРОЙ: РЭЙ";

                }



                break;

            case "com_icon2":
                if (MainScript.ms.player.data.comiscComplete[0])
                {
                    if (MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Рэй")))
                    {
                       
                        HideAllCom();
                        MainScript.ms.fon_com_level.SetActive(false);
                        MainScript.ms.fon_com1.SetActive(true);
                        GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                        GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
                        GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                        MainScript.ms.CurrentCom = 2;
                        if (MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        }
                        else
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(true);

                        }
                        for (int kk = 1; kk <= MainScript.ms.player.data.comicsPos[1]; kk++)
                        {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(true);
                            MainScript.ms.fon_com1.transform.Find("Image").Find("com2_" + kk).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        }
                        if (MainScript.ms.player.data.comicsPos[1] == 11 && !MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                        }
                        if (MainScript.ms.player.data.comicsPos[1] == 18 && !MainScript.ms.player.data.comiscComplete[1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                        }
                        if (MainScript.ms.player.data.comicsPos[1] == 18 && MainScript.ms.player.data.comiscComplete[1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(true);
                            MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(false);
                        }



                    }
                    else
                    {
                        MainScript.ms.fon_no_hero.SetActive(true);
                        MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМ ГЕРОЙ: РЭЙ";

                    }
                }
                else
                {
                    MainScript.ms.fon_no_hero.SetActive(true);
                    MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМО ПРОЙТИ ВСЕ ПРЕДЫДУЩИЕ УРОВНИ";


                }
                break;
            case "com_icon3":
                if (MainScript.ms.player.data.comiscComplete[1]) { 
                if (MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Рэй")))
                {
                       
                        HideAllCom();
                    MainScript.ms.fon_com_level.SetActive(false);
                    MainScript.ms.fon_com1.SetActive(true);
                    GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                    MainScript.ms.CurrentCom = 3;
                        if (MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        }
                        else
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(true);

                        }
                        for (int kk = 1; kk <= MainScript.ms.player.data.comicsPos[2]; kk++)
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("Image").Find("com3_" + kk).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        }
                    if (MainScript.ms.player.data.comicsPos[2] == 16 && !MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                    }
                    if (MainScript.ms.player.data.comicsPos[2] == 18 && !MainScript.ms.player.data.comiscComplete[2])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);

                        }
                        if (MainScript.ms.player.data.comicsPos[2] == 18 && MainScript.ms.player.data.comiscComplete[2])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(false);
                        }



                }
                else
                    {
                        MainScript.ms.fon_no_hero.SetActive(true);
                        MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМЫ ГЕРОИ: РЭЙ, АЙНА";

                    }
                }
                else
                {
                    MainScript.ms.fon_no_hero.SetActive(true);
                    MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМО ПРОЙТИ ВСЕ ПРЕДЫДУЩИЕ УРОВНИ";


                }

                break;
            case "com_icon4":
                if (MainScript.ms.player.data.comiscComplete[2]) { 
                if (MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Рэй")) && MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Лиа")))
                {
                       
                        HideAllCom();
                    MainScript.ms.fon_com_level.SetActive(false);
                    MainScript.ms.fon_com1.SetActive(true);
                    GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                    MainScript.ms.CurrentCom = 4;
                        if (MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        }
                        else
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(true);

                        }
                        for (int kk = 1; kk <= MainScript.ms.player.data.comicsPos[3]; kk++)
                    {
                        if (kk == 8 )
                        {
                            HideAllCom();
                            if (MainScript.ms.player.data.comiscComplete[3])
                             MainScript.ms.fon_com1.transform.Find("button_com_back_page").gameObject.SetActive(true);
                        }

                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("Image").Find("com4_" + kk).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        }
                    if (MainScript.ms.player.data.comicsPos[3] == 6 && !MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                    }
                    if (MainScript.ms.player.data.comicsPos[3] == 16 && !MainScript.ms.player.data.comiscComplete[3])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                        }
                    if (MainScript.ms.player.data.comicsPos[3] == 16 && MainScript.ms.player.data.comiscComplete[3])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(false);
                        }



                }
                else
                    {
                        MainScript.ms.fon_no_hero.SetActive(true);
                        MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМЫ ГЕРОИ: РЭЙ, ЛИА";

                    }
                }
                else
                {
                    MainScript.ms.fon_no_hero.SetActive(true);
                    MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМО ПРОЙТИ ВСЕ ПРЕДЫДУЩИЕ УРОВНИ";


                }

                break;


            case "com_icon5":
                if (MainScript.ms.player.data.comiscComplete[3]) { 
                if (MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Рэй")) && MainScript.ms.player.data.Heros.Contains(MainScript.ms.player.data.Heros.Find(x => x.name == "Лиа")))
                {
                       
                        HideAllCom();
                    MainScript.ms.fon_com_level.SetActive(false);
                    MainScript.ms.fon_com1.SetActive(true);
                    GameObject.Find("fon_com1_text_rating").GetComponent<Text>().text = MainScript.ms.player.data.energy + "/" + MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
                    GameObject.Find("fon_com1_slider_rating").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
                    MainScript.ms.CurrentCom = 5;
                        if (MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1])
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        }
                        else
                        {
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(true);

                        }
                        for (int kk = 1; kk <= MainScript.ms.player.data.comicsPos[4]; kk++)
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("Image").Find("com5_" + kk).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        }
                    if (MainScript.ms.player.data.comicsPos[4] == 11 && !MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(true);
                    }
                    if (MainScript.ms.player.data.comicsPos[4] == 14 && !MainScript.ms.player.data.comiscComplete[4])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("fon_com1_cont").gameObject.SetActive(false);
                        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(true);
                        }
                    if (MainScript.ms.player.data.comicsPos[4] == 14 && MainScript.ms.player.data.comiscComplete[4])
                    {
                        MainScript.ms.fon_com1.transform.Find("fon_com1_exit").gameObject.SetActive(true);
                        MainScript.ms.fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(false);
                            MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(false);
                    }



                }
                else
                    {
                        MainScript.ms.fon_no_hero.SetActive(true);
                        MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМЫ ГЕРОИ: РЭЙ, ЛИА";

                    }
                }
                else
                {
                    MainScript.ms.fon_no_hero.SetActive(true);
                    MainScript.ms.fon_no_hero.transform.GetChild(0).GetComponentInChildren<Text>().text = "ДЛЯ ПРОДОЛЖЕНИЯ НЕОБХОДИМО ПРОЙТИ ВСЕ ПРЕДЫДУЩИЕ УРОВНИ";


                }

                break;

            //еженедельные адания
            case "weekly_but1":
                MainScript.ms.selected_weekly = 0;
                MainScript.ms.UpdateHero();

                MainScript.ms.fon_fight_prepare.SetActive(true);
                for (int m = 0; m < GameObject.Find("fon_fight_prepare_selected").transform.childCount; m++)
                {
                    if (GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.childCount > 0)
                        Destroy(GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.GetChild(0).gameObject);
                }
                MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "НАЧАТЬ СРАЖЕНИЕ";
               // MainScript.ms.fon_fight_prepare.transform.Find("Find_Fight_Timer").gameObject.SetActive(false);
                MainScript.ms.fon_fight_prepare.transform.Find("button_botfight").gameObject.SetActive(false);
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.weekly_type = "9";
                //foreach (Transform child in GameObject.Find("fon_fight_prepare_heroes").transform)
                //{
                //    GameObject.Destroy(child.gameObject);
                //}

                MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();

                //foreach (Hero h in MainScript.ms.player.data.Heros)
                //{
                //    GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
                //    gb_sh.transform.SetParent(GameObject.Find("fon_fight_prepare_heroes").transform, false);
                //    gb_sh.GetComponent<Hero_script>().hero = h;

                //}
                break;
            case "weekly_but2":
                MainScript.ms.selected_weekly = 1;

                MainScript.ms.UpdateHero();

                MainScript.ms.fon_fight_prepare.SetActive(true);
                for (int m = 0; m < GameObject.Find("fon_fight_prepare_selected").transform.childCount; m++)
                {
                    if (GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.childCount > 0)
                        Destroy(GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.GetChild(0).gameObject);
                }
                MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "НАЧАТЬ СРАЖЕНИЕ";
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.weekly_type = "5";
                //foreach (Transform child in GameObject.Find("fon_fight_prepare_heroes").transform)
                //{
                //    GameObject.Destroy(child.gameObject);
                //}

                MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();

                //foreach (Hero h in MainScript.ms.player.data.Heros)
                //{
                //    GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
                //    gb_sh.transform.SetParent(GameObject.Find("fon_fight_prepare_heroes").transform, false);
                //    gb_sh.GetComponent<Hero_script>().hero = h;

                //}
                break;
            case "weekly_but3":
                MainScript.ms.selected_weekly = 2;
                MainScript.ms.UpdateHero();
                MainScript.ms.fon_fight_prepare.SetActive(true);
                for (int m = 0; m < GameObject.Find("fon_fight_prepare_selected").transform.childCount; m++)
                {
                    if (GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.childCount > 0)
                        Destroy(GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.GetChild(0).gameObject);
                }
                MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "НАЧАТЬ СРАЖЕНИЕ";
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.weekly_type = "10";
                //foreach (Transform child in GameObject.Find("fon_fight_prepare_heroes").transform)
                //{
                //    GameObject.Destroy(child.gameObject);
                //}

                MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();

                //foreach (Hero h in MainScript.ms.player.data.Heros)
                //{
                //    GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
                //    gb_sh.transform.SetParent(GameObject.Find("fon_fight_prepare_heroes").transform, false);
                //    gb_sh.GetComponent<Hero_script>().hero = h;

                //}
                break;
            case "weekly_but4":
                MainScript.ms.UpdateHero();
                MainScript.ms.selected_weekly = 3;

                MainScript.ms.fon_fight_prepare.SetActive(true);
                for (int m = 0; m < GameObject.Find("fon_fight_prepare_selected").transform.childCount; m++)
                {
                    if (GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.childCount > 0)
                        Destroy(GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.GetChild(0).gameObject);
                }
                MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "НАЧАТЬ СРАЖЕНИЕ";
                MainScript.ms.fon_weekly.SetActive(false);
                MainScript.ms.weekly_type = "8";
                //foreach (Transform child in GameObject.Find("fon_fight_prepare_heroes").transform)
                //{
                //    GameObject.Destroy(child.gameObject);
                //}

                MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();

                //foreach (Hero h in MainScript.ms.player.data.Heros)
                //{
                //    GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
                //    gb_sh.transform.SetParent(GameObject.Find("fon_fight_prepare_heroes").transform, false);
                //    gb_sh.GetComponent<Hero_script>().hero = h;

                //}
                break;


            case "button_main_but2":

                StartCoroutine(MainScript.ms.FullHP());

                 MainScript.ms.fon_fight_prepare.transform.GetChild(1).GetComponentInChildren<Text>().text = "БОЙ 3X3. ОБЫЧНЫЙ " + Convert.ToChar(13) + "\n <size=12>ВЫБОР ГЕРОЕВ</size> ";

                MainScript.ms.currentWindow = "fon_select_but2";
                MainScript.ms.button_back.SetActive(true);
                MainScript.ms.ComFight = false;
                MainScript.ms.weekly_type = "0";
                MainScript.ms.fon_main.SetActive(false);
                MainScript.ms.fon_select_but2.SetActive(true);

                GameObject.Find("UnlockMoneyText").GetComponent<Text>().text = "Доступно: " + MainScript.ms.player.data.UnlockMoney;
                changeColorButtonMain("");

                
                //MainScript.ms.UpdateHero();
                //MainScript.ms.button_botfight.SetActive(false);

                //MainScript.ms.Find_timer.SetActive(false);


                //MainScript.ms.fon_fight_prepare.SetActive(true);

                //for (int m = 0; m < GameObject.Find("fon_fight_prepare_selected").transform.childCount; m++)
                //{
                //    if (GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.childCount > 0)
                //  Destroy(GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.GetChild(0).gameObject);
                //}

                //GameObject.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "ПОИСК СОПЕРНИКА";
                //MainScript.ms.fon_main.SetActive(false);

                //foreach (Transform child in GameObject.Find("fon_fight_prepare_heroes").transform)
                //{
                //    GameObject.Destroy(child.gameObject);
                //}

                //MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();

                //foreach (Hero h in MainScript.ms.player.data.Heros)
                //{
                //    GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
                //    gb_sh.transform.SetParent(GameObject.Find("fon_fight_prepare_heroes").transform, false);
                //    gb_sh.GetComponent<Hero_script>().hero = h;

                //}




                break;

            case "button_find_invites":
                foreach (Transform n in MainScript.ms.menu_chat_invites.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                {
                    if (MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text != "")
                    {
                        if (n.name.Contains(MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text))
                        {
                            n.gameObject.SetActive(true);
                        }
                        else
                        {
                            n.gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        n.gameObject.SetActive(true);
                    }
                }
                break;

            case "button_find_friends":

               

                break;

            case "button_find":
                if (MainScript.ms.menu_chat_poisk.transform.Find("input_name").GetComponent<InputField>().text.Trim() != "")
                {
                    var findPlayer = new WWW(MainScript.ms.server_url + "search_user.php?q=" + MainScript.ms.menu_chat_poisk.transform.Find("input_name").GetComponent<InputField>().text);
                    while (!findPlayer.isDone) ;
                    var fd = JsonUtility.FromJson<findPlayers>(findPlayer.text);
                    MainScript.ms.finded = fd;
                    foreach(Transform qwe in MainScript.ms.menu_chat_poisk.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                    {
                        Destroy(qwe.gameObject);
                    }
                    foreach (findsPl n in fd.users )
                    {
                        if (n.login != MainScript.ms.player.login && MainScript.ms.player.data.SendsRequests.FindIndex(x => x.name == n.login) ==-1 && MainScript.ms.player.data.friends.FindIndex(x => x.name == n.login) == -1)
                        {
                            var asd = Instantiate(MainScript.ms.ItemFind, MainScript.ms.menu_chat_poisk.transform.Find("Scroll View").Find("Viewport").Find("Content").transform);
                            asd.GetComponent<FindedPlayer>().id = n.pid;
                            asd.GetComponent<FindedPlayer>().name = n.login;
                            asd.GetComponent<FindedPlayer>().pass = n.pass;
                            asd.transform.Find("Text").GetComponent<Text>().text = n.login;
                        }
                    }
                }

                break;

            case "buttonAdd":
                if (GameObject.Find("menu_chat_poisk"))
                {
                    WWW Find = new WWW(MainScript.ms.server_url + "get_login.php?login=" + transform.GetComponentInParent<FindedPlayer>().name + " &pass=" + transform.GetComponentInParent<FindedPlayer>().pass);
                    while (!Find.isDone) ;

                    requestFriend rF = new requestFriend
                    {
                        pid = MainScript.ms.player.pid,
                        name = MainScript.ms.player.login,
                        pass = MainScript.ms.player.pass
                    };

                    MainScript.ms.FindedPlayer = JsonUtility.FromJson<Player>(Find.text);
                    MainScript.ms.FindedPlayer.data.requestFriends.Add(rF);

                    SendsRequest SQ = new SendsRequest
                    {
                        pid = MainScript.ms.FindedPlayer.pid,
                        name = MainScript.ms.FindedPlayer.login,
                        pass = MainScript.ms.FindedPlayer.pass
                    };

                    MainScript.ms.player.data.SendsRequests.Add(SQ);

                    new WWW(MainScript.ms.server_url + "get_login.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&data=" + JsonUtility.ToJson(MainScript.ms.player.data));

                    new WWW(MainScript.ms.server_url + "get_login.php?login=" + MainScript.ms.FindedPlayer.login + "&pass=" + MainScript.ms.FindedPlayer.pass + "&data=" + JsonUtility.ToJson(MainScript.ms.FindedPlayer.data));

                    Destroy(transform.parent.gameObject);
                }
                if (GameObject.Find("menu_chat_invites"))
                {
                    friends fr = new friends
                    {
                        pid = MainScript.ms.player.data.requestFriends.Find(x => x.pid == transform.parent.GetComponent<FindedPlayer>().id).pid,
                        name = MainScript.ms.player.data.requestFriends.Find(x => x.pid == transform.parent.GetComponent<FindedPlayer>().id).name,
                        pass = MainScript.ms.player.data.requestFriends.Find(x => x.pid == transform.parent.GetComponent<FindedPlayer>().id).pass
                    };

                    MainScript.ms.player.data.friends.Add(fr);

                    requestFriend rF = new requestFriend
                    {
                        pid = MainScript.ms.player.data.requestFriends.Find(x => x.pid == transform.parent.GetComponent<FindedPlayer>().id).pid,
                        name = MainScript.ms.player.data.requestFriends.Find(x => x.pid == transform.parent.GetComponent<FindedPlayer>().id).name,
                        pass = MainScript.ms.player.data.requestFriends.Find(x => x.pid == transform.parent.GetComponent<FindedPlayer>().id).pass
                    };

                    MainScript.ms.player.data.requestFriends.Remove(MainScript.ms.player.data.requestFriends.Single(x => x.pid == MainScript.ms.player.data.requestFriends.Find(x1 => x1.pid == transform.parent.GetComponent<FindedPlayer>().id).pid));


                    WWW Find = new WWW(MainScript.ms.server_url + "get_login.php?login=" + transform.GetComponentInParent<FindedPlayer>().name + " &pass=" + transform.GetComponentInParent<FindedPlayer>().pass);
                    while (!Find.isDone) ;

                    fr = new friends
                    {
                        pid = MainScript.ms.player.pid,
                        name = MainScript.ms.player.login,
                        pass = MainScript.ms.player.pass
                    };

                    SendsRequest SQ = new SendsRequest
                    {
                        pid = MainScript.ms.player.pid,
                        name = MainScript.ms.player.login,
                        pass = MainScript.ms.player.pass
                    };

                    MainScript.ms.FindedPlayer = JsonUtility.FromJson<Player>(Find.text);
                    MainScript.ms.FindedPlayer.data.SendsRequests.Remove(MainScript.ms.FindedPlayer.data.SendsRequests.Single(x => x.pid == MainScript.ms.player.pid));
                    MainScript.ms.FindedPlayer.data.friends.Add(fr);

                    new WWW(MainScript.ms.server_url + "get_login.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&data=" + JsonUtility.ToJson(MainScript.ms.player.data));

                    new WWW(MainScript.ms.server_url + "get_login.php?login=" + MainScript.ms.FindedPlayer.login + "&pass=" + MainScript.ms.FindedPlayer.pass + "&data=" + JsonUtility.ToJson(MainScript.ms.FindedPlayer.data));

                    Destroy(transform.parent.gameObject);
                }

                break;

            case "ButtonFindFight":

                var fon1 = UnityEngine.Random.Range(0, MainScript.ms.allFons.Count - 1);

                MainScript.ms.fon_fight.GetComponent<Image>().sprite = MainScript.ms.allFons[fon1];

                if ((GameObject.Find("slot1").GetComponentInChildren<Hero_script>())
                    || (GameObject.Find("slot2").GetComponentInChildren<Hero_script>())
                    || (GameObject.Find("slot3").GetComponentInChildren<Hero_script>()))
                {
                    if (!MainScript.ms.Find)
                    {
                        if (
                            GameObject.Find("slot1").GetComponentInChildren<Hero_script>()
                            && GameObject.Find("slot2").GetComponentInChildren<Hero_script>()
                            && GameObject.Find("slot3").GetComponentInChildren<Hero_script>()

                            )
                        {
                            Debug.Log(Convert.ToInt16(GameObject.Find("slot1").GetComponentInChildren<Hero_script>().hero.type));
                            Debug.Log(Convert.ToInt16(GameObject.Find("slot2").GetComponentInChildren<Hero_script>().hero.type));
                            Debug.Log(Convert.ToInt16(GameObject.Find("slot3").GetComponentInChildren<Hero_script>().hero.type));

                            MainScript.ms.player.data.selected_heroes[0] = Convert.ToInt16(GameObject.Find("slot1").GetComponentInChildren<Hero_script>().hero.type);
                            MainScript.ms.player.data.selected_heroes[1] = Convert.ToInt16(GameObject.Find("slot2").GetComponentInChildren<Hero_script>().hero.type);
                            MainScript.ms.player.data.selected_heroes[2] = Convert.ToInt16(GameObject.Find("slot3").GetComponentInChildren<Hero_script>().hero.type);
                            MainScript.ms.Player_data_save();
                            //MainScript.ms.player.data.selected_heroes[0] = MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot1").GetComponentInChildren<Hero_script>().hero);
                            //MainScript.ms.player.data.selected_heroes[1] = MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot2").GetComponentInChildren<Hero_script>().hero);
                            //MainScript.ms.player.data.selected_heroes[2] = MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot3").GetComponentInChildren<Hero_script>().hero);
                            //  Debug.Log(MainScript.ms.player.data.Heros.IndexOf(GameObject.Find("slot1").GetComponentInChildren<Hero_script>().hero));
                            if (MainScript.ms.weekly_type == "0")
                                MainScript.ms.find_fight = StartCoroutine(MainScript.ms.Find_fight());
                            else
                            {

                                if (MainScript.ms.player.data.energy >= 10)
                                {
                                    if (MainScript.ms.selected_weekly == 0)
                                    {
                                        weekly_mobs(MainScript.ms.weekly_type,1-1,1 - 1, 2 - 1);
                                    }
                                    if (MainScript.ms.selected_weekly == 1)
                                    {
                                        weekly_mobs(MainScript.ms.weekly_type,2 - 1, 2 - 1, 3 - 1);
                                    }

                                    if (MainScript.ms.selected_weekly == 2)
                                    {
                                        weekly_mobs(MainScript.ms.weekly_type,3 - 1, 3 - 1, 4 - 1);
                                    }
                                    if (MainScript.ms.selected_weekly == 3)
                                    {
                                        weekly_mobs(MainScript.ms.weekly_type,4 - 1, 4 - 1, 5 - 1);
                                    }
                                    MainScript.ms.player.data.energy -= 10;
                                }
                                else
                                {
                                    MainScript.ms.fon_no_energy.SetActive(true);
                                }
                            }

                        }
                       // else MainScript.ms.ShowMessage("Выберите героев", "Перетащите в свободные окна героев для боя!", "OK");
                    }
                    else
                    {
                        MainScript.ms.Find = false;
                        StopCoroutine(MainScript.ms.find_fight);
                        MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").GetComponent<Animator>().Play("close");
                        MainScript.ms.button_botfight.SetActive(false);
                        MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "ПОИСК СОПЕРНИКА";

                    }
                }
                break;


            case "button_heroes_close":

                MainScript.ms.fon_heroes_one.gameObject.SetActive(false);
                break;

            case "button_heroes_close_buy":

                MainScript.ms.fon_heroes_buy_window.gameObject.SetActive(false);
                break;


            case "button_heal":

                if ((MainScript.ms.player.data.Heros[Hero_about.self.hero_id].maxHp - MainScript.ms.player.data.Heros[Hero_about.self.hero_id].heals / 20) < MainScript.ms.player.data.gold)
                {
                    MainScript.ms.player.data.gold -= ((MainScript.ms.player.data.Heros[Hero_about.self.hero_id].maxHp - MainScript.ms.player.data.Heros[Hero_about.self.hero_id].heals) / 20);
                    MainScript.ms.player.data.Heros[Hero_about.self.hero_id].heals = MainScript.ms.player.data.Heros[Hero_about.self.hero_id].maxHp;

                }


                break;


            case "button_upgrade":


                Hero her = MainScript.ms.player.data.Heros.Find(x => x.type == Hero_about.self.hero_id.ToString());
                if (her.lvl < 10 &&
                    her.damageAll == MainScript.ms.lvl_up_val[her.lvl - 1])
                {
                    her.lvl += 1;

                    her.maxHp = Convert.ToInt32(her.maxHp * 1.7f);
                    her.heals = her.maxHp;
                    her.crit_persent += 0.5f;
                    her.dodge_persent += 0.5f;
                    her.damage = Convert.ToInt32(her.damage * 1.5f);

                    //NEED SAVE
                    foreach(Hero_script n in MainScript.ms.heroes)
                    {
                        n.lvl.text = n.hero.lvl+"";
                    }
                    if (MainScript.ms.fon_heroes.activeSelf)
                    {
                        MainScript.ms.UpdateHero();
                    }
                    Hero_about.self.Show_about(Hero_about.self.hero_id);

                    MainScript.ms.ShowMessage(her.name + " повысил уровень! ", "до " + her.lvl, "Ок");

                }



                break;






            case "fon_com1_cont":
                MainScript.ms.fon_com1.gameObject.SetActive(false);
                MainScript.ms.fon_com_level.SetActive(true);
                MainScript.ms.Fon_main_update();
                break;



            case "button_exit":
                MainScript.ms.fon_com1.gameObject.SetActive(false);
                if (!MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1])
                {
                    MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1] = 0;
                    MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1] = false;
                }
                MainScript.ms.fon_com_level.SetActive(true);
                MainScript.ms.Fon_main_update();
               // MainScript.ms.fon_com_level.SetActive(true);
                MainScript.ms.fon_com1.transform.Find("confirm_leave").gameObject.SetActive(false);

                break;

            case "button_cance":
                MainScript.ms.fon_com1.transform.Find("confirm_leave").gameObject.SetActive(false);

                break;

            case "fon_com1_exit":
                if (!MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom-1])
                    MainScript.ms.fon_com1.transform.Find("confirm_leave").gameObject.SetActive(true);
                else
                {
                    MainScript.ms.fon_com1.SetActive(false);
                    MainScript.ms.fon_com_level.SetActive(true);
                   // MainScript.ms.fon_main.SetActive(true);
                    MainScript.ms.Fon_main_update();
                    MainScript.ms.fon_com1.transform.Find("confirm_leave").gameObject.SetActive(false);

                }
                //MainScript.ms.fon_com1.gameObject.SetActive(false);
                //if (!MainScript.ms.player.data.comiscComplete[MainScript.ms.CurrentCom - 1])
                //{
                //    MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1] = 0;
                //    MainScript.ms.player.data.comiscKillBot[MainScript.ms.CurrentCom - 1] = false;
                //}
                //MainScript.ms.Fon_main_update();
                break;




            case "fon_com1_end": //Кнопка "Закончить" после комикса

                break;

        }


    }

    public void Hide()
    {
        if (!MainScript.ms.Find)
        MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").gameObject.SetActive(false);
    }

    public void weekly_mobs(string type, int lvl1, int lvl2, int lvl3)
    {
        StartCoroutine(MainScript.ms.StartFight());




        Player bot = new Player();
        bot.data = new player_data();
        bot.data.lvl = 1;

        bot.login = "Бот";
        bot.isbot = true;

        bot.data.Heros = new List<Hero>();

        bot.data.Heros.Add(new Hero
        {
            name = "Рэй1",
            type = type,
            lvl = 1,
            heals = MainScript.ms.hp_in_lvl[lvl1],
            damage = MainScript.ms.damage_in_lvl[lvl1],
            crit_persent = 0.5f,
            dodge_persent = 0.5f,
            maxHp = MainScript.ms.hp_in_lvl[lvl1],
            damageAll = 0
        });
        bot.data.Heros.Add(new Hero
        {
            name = "Рэй2",
            type = type,
            lvl = 1,
            heals = MainScript.ms.hp_in_lvl[lvl2],
            damage = MainScript.ms.damage_in_lvl[lvl2],
            crit_persent = 0.5f,
            dodge_persent = 0.5f,
            maxHp = MainScript.ms.hp_in_lvl[lvl2],
            damageAll = 0
        });
        bot.data.Heros.Add(new Hero
        {
            name = "Рэй3",
            type = type,
            lvl = 1,
            heals = MainScript.ms.hp_in_lvl[lvl3],
            damage = MainScript.ms.damage_in_lvl[lvl3],
            crit_persent = 0.5f,
            dodge_persent = 0.5f,
            maxHp = MainScript.ms.hp_in_lvl[lvl3],
            damageAll = 0
        });

        //                bot.data.Heros.Add(MainScript.ms.player.data.Heros[MainScript.ms.player.data.selected_heroes[1]]);    
        



        MainScript.ms.enemy = bot;

        MainScript.ms.enemy.data.selected_heroes[0] = 0;
        MainScript.ms.enemy.data.selected_heroes[1] = 1;
        MainScript.ms.enemy.data.selected_heroes[2] = 2;


        MainScript.ms.internal_fight = new Fight();
        MainScript.ms.internal_fight.players = new List<Player>();
        MainScript.ms.internal_fight.actions_list = new List<Action>();

        MainScript.ms.internal_fight.players.Add(MainScript.ms.player);
        MainScript.ms.internal_fight.players.Add(MainScript.ms.enemy);

        MainScript.ms.fon_fight_finder.gameObject.SetActive(false);
        MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);

        MainScript.ms.Begin_fight();
    }

    public void pvp(){
        MainScript.ms.fon_select_but2.SetActive(false);
        MainScript.ms.UpdateHero();
        MainScript.ms.button_botfight.SetActive(false);

       // MainScript.ms.Find_timer.SetActive(false);


        MainScript.ms.fon_fight_prepare.SetActive(true);

        for (int m = 0; m < GameObject.Find("fon_fight_prepare_selected").transform.childCount; m++)
        {
            if (GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.childCount > 0)
                Destroy(GameObject.Find("fon_fight_prepare_selected").transform.GetChild(m).transform.GetChild(0).gameObject);
        }

        GameObject.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "ПОИСК СОПЕРНИКА";
        MainScript.ms.fon_main.SetActive(false);

        if (GameObject.Find("fon_fight_prepare_heroes"))
            foreach (Transform child in GameObject.Find("fon_fight_prepare_heroes").transform)
            {
                GameObject.Destroy(child.gameObject);
            }

        MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();

        foreach (Hero h in MainScript.ms.player.data.Heros)
        {
            GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
            gb_sh.transform.SetParent(GameObject.Find("fon_fight_prepare_heroes").transform, false);
            gb_sh.GetComponent<Hero_script>().hero = h;

        }
    }

    public void HideAllCom()
    {
        MainScript.ms.fon_com1.transform.Find("button_com_fight").gameObject.SetActive(false);
        MainScript.ms.fon_com1.transform.Find("button_com_reward").gameObject.SetActive(false);
        foreach(Transform n in MainScript.ms.fon_com1.transform.Find("Image").transform)
        {
            n.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }
        MainScript.ms.fon_com1.transform.Find("button_com_next_page").gameObject.SetActive(false);
        MainScript.ms.fon_com1.transform.Find("button_com_back_page").gameObject.SetActive(false);
    }

public void OnMouseExit()
    {


        if (gameObject.GetComponent<Image>())
            gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);


    }
    public void OnMouseOver()
    {


        if (gameObject.GetComponent<Image>())
            gameObject.GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f, 1f);





    }


   



    IEnumerator curcom()
    {
        if (MainScript.ms.CurrentCom == 1)
        {
            var gg = GameObject.Find("com1_" + MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1]);

            //while (gg.GetComponent<Image>().color.a < 1f)
            //{

            //gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, gg.GetComponent<Image>().color.a + 0.01f);
            gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(0.01f);

            // }
        }

        if (MainScript.ms.CurrentCom == 2)
        {
            var gg = GameObject.Find("com2_" + MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1]);

            //while (gg.GetComponent<Image>().color.a < 1f)
            //{

            //gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, gg.GetComponent<Image>().color.a + 0.01f);
            gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(0.01f);

            // }
        }
        if (MainScript.ms.CurrentCom == 3)
        {
            var gg = GameObject.Find("com3_" + MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1]);

            //while (gg.GetComponent<Image>().color.a < 1f)
            //{

            //gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, gg.GetComponent<Image>().color.a + 0.01f);
            gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(0.01f);

            // }
        }
        if (MainScript.ms.CurrentCom == 4)
        {
            var gg = GameObject.Find("com4_" + MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1]);
            if(MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1] == 8)
            {
                HideAllCom();
            }
            if (!MainScript.ms.fon_com1.transform.Find("button_com_next_page").gameObject.activeSelf)
            {
                gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                yield return new WaitForSeconds(0.01f);
            }


        }
        if (MainScript.ms.CurrentCom == 5)
        {
            var gg = GameObject.Find("com5_" + MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1]);

            //while (gg.GetComponent<Image>().color.a < 1f)
            //{

            //gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, gg.GetComponent<Image>().color.a + 0.01f);
            gg.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(0.01f);

        }

    }

    public IEnumerator waiting_button()
    {
        yield return new WaitForSeconds(1);
        MainScript.ms.fon_end_quest.transform.GetChild(0).GetChild(3).gameObject.SetActive(true);
    }



}



