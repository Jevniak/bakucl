﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.Threading;


using System.Net;
using System.Text;


using UnityEngine;
using UnityEngine.UI;


using System.Xml;
using System.IO;
using System.Globalization;

[Serializable]
public class player_data
{
    

    public int lvl;
    public int gold;
    public int life;
    public int rating;
    public string lang;
    public bool music_on;
    public bool sound_on;
    public int pobed;
    public int damageAll;
    public int energy;
    public int energy_max;
    public int[] comicsPos = { 0, 0, 0, 0, 0 };
    public bool[] comiscKillBot = { false, false, false, false, false };
    public bool[] comiscComplete = { false, false, false, false, false };
    public int[] selected_heroes = { -1, -1, -1 };

    public bool[] weekly_win = { false, false, false, false };

    public int count_books;
    public int count_sheets;

    public byte CountLists;
    public byte CountBooks;

    public string LastJoinDate;
    public string UnlockMoneyDate;
    public byte UnlockMoney;

    public List<requestFriend> requestFriends;
    public List<SendsRequest> SendsRequests;
    public List<friends> friends;

    public List<Shop_item> shop_items;

    public List<Hero> Heros;

}

[Serializable]
public class SendsRequest
{
    public int pid;
    public string name;
    public string pass;
}

[Serializable]
public class requestFriend
{
    public int pid;
    public string name;
    public string pass;
}

[Serializable]
public class friends
{
    public int pid;
    public string name;
    public string pass;
}

[Serializable]
public class Fight
{
    public List<Player> players;
    public List<Action> actions_list;


}


[Serializable]
public class Shop_item
{
    public string name;
    public string art;
    public int cost;

}




[Serializable]
public class Action
{
    public string name;
    public string value;
    public int pid1;
    public int pid2;
}




[Serializable]
public class Hero
{
    public string name;
    public string type;
    public int lvl;
    public int damage;
    public float crit_persent;
    public float dodge_persent;
    public int heals;
    public int maxHp;
    public int damageAll;
    public string description;
}







[Serializable]
public class Player
{
    public int pid;
    public string login;
    public string pass;
    public player_data data;
    public int error;
    public bool isbot = false;
}


[Serializable]
public class Skill_correction
{
    public float damage = 1;
    public float dodge = 20;
    public float curse = 1;
    public float magic_damage = 1;
    public float crit = 20;

}



[Serializable]
public class Chat_message
{
    public int chat_id;
    public int user_id;
    public int to_id;
    public string text;
    public string date;
    public string login;

}


[Serializable]
public class Chat_messages
{
    public List<Chat_message> chat_messages;

}

[Serializable]
public class findPlayers
{
    public List<findsPl> users;
}
[Serializable]
public class findsPl
{
    public int pid;
    public string login;
    public string pass;
}




public class MainScript : MonoBehaviour
{
    [Multiline]
    public string[] all_description;

    public int[] hp_in_lvl = {400,680,1156,1965,3340};
    public int[] damage_in_lvl = {20,30,45,67,100};

    public GameObject Content_for_win;

    public GameObject Content_for_reward_comics;
    public GameObject prefab_reward;

    /*
    0 - золото
    1 - книга  
    2 - листы  
    */

    public List<Sprite> reward_icons;

    public List<Sprite> allFons;
    #region эффекты

    public GameObject ContentPlayer1;
    public GameObject ContentPlayer2;

    public GameObject PrefabEffect;

    public Sprite circleDemon;
    public Sprite circleDerevo;
    public Sprite circleKamen;
    public Sprite circleMaamyn;
    public Sprite circleNoil;
    public Sprite circleGhost;

    public Sprite Red;
    public Sprite Green;
    public Sprite Purple;
    public Sprite Blue;


    #endregion
    public List<Hero_script> heroes;

    public int selected_weekly;

    public GameObject fon_heroes_buy;

    public Sprite button1_normal;
    public Sprite button23_normal;
    public Sprite button4_normal;

    public Sprite button1_active;
    public Sprite button23_active;
    public Sprite button4_active;


    public bool GainMoney = false;

    public bool pause = false;

    public GameObject ItemFind;
    public findPlayers finded;
    public Player FindedPlayer;

    public bool ComFight = false;
    public int CurrentCom;

    public string currentWindow;
    public GameObject button_back;

    public GameObject fon_no_energy;
    public GameObject fon_no_hero;
    public void UpdateHero()
    {


        MainScript.ms.player.data.Heros = MainScript.ms.player.data.Heros.OrderByDescending(x => x.lvl).ToList<Hero>();


        foreach (Transform child in MainScript.ms.fon_heroes_content.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in fon_fight_prepare.transform.Find("Scroll View").Find("Viewport").Find("Content").Find("fon_fight_prepare_heroes").transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in fon_fight_prepare.transform.Find("fon_fight_prepare_selected"))
        {
            if (child.childCount > 0)
            GameObject.Destroy(child.GetChild(0).gameObject);
        }

        fon_fight_prepare.transform.Find("block_Timer").gameObject.SetActive(false);
        fon_fight_prepare.transform.Find("button_botfight").gameObject.SetActive(false);
        fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "ПОИСК СОПЕРНИКА";
        heroes.Clear();
        foreach (Hero h in MainScript.ms.player.data.Heros)
        {

            if (lvl_up_val[h.lvl - 1] < h.damageAll) h.damageAll = lvl_up_val[h.lvl - 1];
            if (MainScript.ms.fon_heroes.activeSelf)
            {
                Debug.Log("1");
                GameObject gb_sh = Instantiate(Resources.Load<GameObject>("hero"));
                gb_sh.transform.SetParent(MainScript.ms.fon_heroes_content.transform, false);
                gb_sh.GetComponent<Hero_script>().hero = h;
                heroes.Add(gb_sh.GetComponent<Hero_script>());
            }
            else { 
                Debug.Log("2");

                GameObject gb_sh1 = Instantiate(Resources.Load<GameObject>("heroSel"));
                gb_sh1.transform.SetParent(fon_fight_prepare.transform.Find("Scroll View").Find("Viewport").Find("Content").Find("fon_fight_prepare_heroes").transform, false);
                gb_sh1.GetComponent<Hero_script>().hero = h;
                heroes.Add(gb_sh1.GetComponent<Hero_script>());

            }
        }






    }

    public List<Text> buttons_com_levels;

    public void Player_data_save()
    {
        new WWW(MainScript.ms.server_url + "get_login.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&data=" + JsonUtility.ToJson(MainScript.ms.player.data));
    }

    public GameObject fon_end_quest;

    public void UpdateShop()
    {


        foreach (Transform child in MainScript.ms.fon_shop.transform.Find("Scroll View").transform.Find("Viewport").transform.Find("Content"))
        {
            GameObject.Destroy(child.gameObject);
        }


        foreach (Shop_item si in MainScript.ms.player.data.shop_items)
        {

            GameObject gb_sh = Instantiate(Resources.Load<GameObject>("shopitem"));

            gb_sh.transform.SetParent(MainScript.ms.fon_shop.transform.Find("Scroll View").transform.Find("Viewport").transform.Find("Content"), false);
            gb_sh.GetComponent<Image>().sprite = Resources.Load<Sprite>("shop/her_ico" + si.art.Replace("hero", ""));
            gb_sh.GetComponent<shop_item>().cur_shop_item = si;

        }


    }

    public bool Find = false;

    public Coroutine find_fight;

    public Fight Fight1;

    public IEnumerator Find_fight()
    {

        WWW www;
        //#if UNITY_EDITOR
        //        www = new WWW(MainScript.ms.server_url + "get_fight.php?clear");
        //        while (!www.isDone) ;


        //#endif
        if (raiting) www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=true");
        else www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=false");
        while (!www.isDone) ;

        //MainScript.ms.fon_fight_finder.gameObject.SetActive(true);
        MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").gameObject.SetActive(true);
        MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").GetComponent<Animator>().Play("open");


        int timer = 0;

        Find = true;

        MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().text = "ПРЕКРАТИТЬ ПОИСК";

        int c = 0;
        byte min = 0;
        while (c == 0)
        {
            //if (!Find)
            //{
            //    MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").gameObject.SetActive(false);
            //    MainScript.ms.button_botfight.SetActive(false);
            //    break;
            //}
            //MainScript.ms.fon_fight_finder.gameObject.GetComponentInChildren<Text>().text = "00:0" + timer;

            if (timer > 9) MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").Find("Find_Fight_Timer").GetComponent<Text>().text = min + ":" + timer;
            else MainScript.ms.fon_fight_prepare.transform.Find("block_Timer").Find("Find_Fight_Timer").GetComponent<Text>().text = min + ":0" + timer;
            timer++;
            if (timer > 59)
            {
                timer = 0;
                min++;
            }

            if ((timer > 10 || min > 0) && !raiting) MainScript.ms.button_botfight.SetActive(true);

            /*
            #if UNITY_EDITOR
                        if (timer == 8)
                        {
                            www = new WWW(MainScript.ms.server_url + "get_fight.php?login=w2&pass=1&enemy_id=-1");
                            while (!www.isDone) ;
                        }
            #endif

                */



            if (raiting)
                www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&raiting=true");
            else
                www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&raiting=false");

            while (!www.isDone) ;


             Fight1 = JsonUtility.FromJson<Fight>(www.text);

            foreach (Player cur_player in Fight1.players)


                if (MainScript.ms.player.pid != cur_player.pid && cur_player.pid > 0)
                {
                    MainScript.ms.enemy = cur_player;
                    //MainScript.ms.enemy.data.selected_heroes = new int[] { 0, 1, 2 };
                    MainScript.ms.enemy.data.selected_heroes = new int[] { cur_player.data.selected_heroes[0], cur_player.data.selected_heroes[1], cur_player.data.selected_heroes[2] };
                    c = 1;
                }


            yield return new WaitForSeconds(1f);


        }

        if (c == 1)
        {
            //www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "");

            if (raiting)
                www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&raiting=true");
            else
                www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&raiting=false");


            while (!www.isDone) ;

            Fight1 = JsonUtility.FromJson<Fight>(www.text);

            foreach (Player cur_player in Fight1.players)


                if (MainScript.ms.player.pid != cur_player.pid && cur_player.pid > 0)
                {
                    MainScript.ms.enemy = cur_player;
                    //MainScript.ms.enemy.data.selected_heroes = new int[] { 0, 1, 2 };
                    MainScript.ms.enemy.data.selected_heroes = new int[] { cur_player.data.selected_heroes[0], cur_player.data.selected_heroes[1], cur_player.data.selected_heroes[2] };
                    c = 1;
                }
            MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);

            MainScript.ms.fon_fight_finder.gameObject.SetActive(false);
            MainScript.ms.Begin_fight();
        }

        else
        {


        }

    }

    public IEnumerator UpdateInfo()
    {
        //if (MainScript.ms.currentWindow != "select_com_weekly")
        //{
            MainScript.ms.fon_fight_prepare.transform.Find("ButtonFindFight").GetComponentInChildren<Text>().color = new Color(0.5f, 0.5f, 0.5f);
        //}
        int curent_lvl = 0;
        for (curent_lvl = 0; curent_lvl < player_lvl_up_val.Count && player_lvl_up_val[curent_lvl] <= MainScript.ms.player.data.damageAll; curent_lvl++) ;
        MainScript.ms.player.data.lvl = (curent_lvl + 1);

        if (GameObject.Find("player_gold"))
            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);

        if (GameObject.Find("player_gold"))
            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = MainScript.ms.player.data.gold + "";

        if (GameObject.Find("player_name"))
            GameObject.Find("player_name").GetComponent<Text>().text = "[" + MainScript.ms.player.data.lvl + "] " + MainScript.ms.player.login;

        if (GameObject.Find("player_energy"))
            GameObject.Find("player_energy").GetComponent<Text>().text = "ЭНЕРГИЯ " + MainScript.ms.player.data.energy + "/<size=+21>" + MainScript.ms.player.data.energy_max + "</size>";

        if (GameObject.Find("player_energy_bar"))
        {
            GameObject.Find("player_energy_bar").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
            GameObject.Find("player_energy_bar").GetComponent<Slider>().value = MainScript.ms.player.data.energy;
        }

        if (GameObject.Find("player_uron"))
        {

            if (MainScript.ms.player.data.damageAll < 9000)
                GameObject.Find("player_uron").GetComponent<Text>().text = "ОПЫТ  " + MainScript.ms.player.data.damageAll + "/<size=+21>" + player_lvl_up_val[MainScript.ms.player.data.lvl - 1] + "</size>";
            else
                GameObject.Find("player_uron").GetComponent<Text>().text = "ОПЫТ 9000/<size=+21>9000</size>";
        }

        if (GameObject.Find("player_uron_bar"))
        {
            if (MainScript.ms.player.data.damageAll < 9000)
            {
                GameObject.Find("player_uron_bar").GetComponent<Slider>().maxValue = player_lvl_up_val[MainScript.ms.player.data.lvl - 1];
                GameObject.Find("player_uron_bar").GetComponent<Slider>().value = MainScript.ms.player.data.damageAll;
            }
            else
            {
                GameObject.Find("player_uron_bar").GetComponent<Slider>().maxValue = 9000;
                GameObject.Find("player_uron_bar").GetComponent<Slider>().value = 9000;
            }
        }



        if (GameObject.Find("player_rating"))
            GameObject.Find("player_rating").GetComponent<Text>().text = "Рейтинг: " + MainScript.ms.player.data.rating;

        if (GameObject.Find("player_pobed"))
            GameObject.Find("player_pobed").GetComponent<Text>().text = "Побед: " + MainScript.ms.player.data.pobed;

        if (GameObject.Find("player_kolvogeroev"))
            GameObject.Find("player_kolvogeroev").GetComponent<Text>().text = "Количество героев: " + MainScript.ms.player.data.Heros.Count();

        foreach(Hero n in player.data.Heros)
        {
            n.heals = n.maxHp;
        }

        int tmp_lvl = 0;
        foreach (Hero heroes in MainScript.ms.player.data.Heros) tmp_lvl += heroes.lvl;

        if (GameObject.Find("player_lvl_allHeroes"))
            GameObject.Find("player_lvl_allHeroes").GetComponent<Text>().text = "Общий уровень героев: " + tmp_lvl;


        

        if (MainScript.ms.player.data.energy_max > MainScript.ms.player.data.energy)
        {

            //MainScript.ms.button_main_energy_buy.GetComponentInChildren<Text>().text = (MainScript.ms.player.data.energy_max - MainScript.ms.player.data.energy) + " GOLD";
            MainScript.ms.button_main_energy_buy.SetActive(true);
        }
        else
            MainScript.ms.button_main_energy_buy.SetActive(false);
        yield return null;
    }

    public void Fon_main_update()
    {

        int curent_lvl = 0;
        for (curent_lvl = 0; curent_lvl < player_lvl_up_val.Count && player_lvl_up_val[curent_lvl] <= MainScript.ms.player.data.damageAll; curent_lvl++) ;
        MainScript.ms.player.data.lvl = (curent_lvl + 1);
       // fon_main.SetActive(true);


        if (GameObject.Find("player_gold"))
            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = MainScript.ms.player.data.gold + "";

        if (fon_main.activeSelf)
        {
            GameObject.Find("player_name").GetComponent<Text>().text = "[" + MainScript.ms.player.data.lvl + "] " + MainScript.ms.player.login;

            GameObject.Find("player_energy").GetComponent<Text>().text = "ЭНЕРГИЯ " + MainScript.ms.player.data.energy + "/<size=+21>" + MainScript.ms.player.data.energy_max + "</size>";


            GameObject.Find("player_energy_bar").GetComponent<Slider>().maxValue = MainScript.ms.player.data.energy_max;
            GameObject.Find("player_energy_bar").GetComponent<Slider>().value = MainScript.ms.player.data.energy;


            if (player.data.lvl < 10)
            {
                GameObject.Find("player_uron").GetComponent<Text>().text = "ОПЫТ  " + MainScript.ms.player.data.damageAll + "/<size=+21>" + player_lvl_up_val[MainScript.ms.player.data.lvl - 1] + "</size>";
                GameObject.Find("player_uron_bar").GetComponent<Slider>().maxValue = player_lvl_up_val[MainScript.ms.player.data.lvl - 1];
                GameObject.Find("player_uron_bar").GetComponent<Slider>().value = MainScript.ms.player.data.damageAll;

            }
            else
            {
                GameObject.Find("player_uron").GetComponent<Text>().text = "ОПЫТ 9000/<size=+21>9000</size>";

            }
        }
        if (fon_main.activeSelf)
        {
            GameObject.Find("player_rating").GetComponent<Text>().text = "Рейтинг: " + MainScript.ms.player.data.rating;
            GameObject.Find("player_pobed").GetComponent<Text>().text = "Побед: " + MainScript.ms.player.data.pobed;
            GameObject.Find("player_kolvogeroev").GetComponent<Text>().text = "Количество героев: " + MainScript.ms.player.data.Heros.Count();



            int tmp_lvl = 0;
            foreach (Hero heroes in MainScript.ms.player.data.Heros) tmp_lvl += heroes.lvl;

            GameObject.Find("player_lvl_allHeroes").GetComponent<Text>().text = "Общий уровень героев: " + tmp_lvl;



            GameObject.Find("player_gold").GetComponentInChildren<Text>().text = Convert.ToString(MainScript.ms.player.data.gold);

            if (MainScript.ms.player.data.energy_max > MainScript.ms.player.data.energy)
            {

                //MainScript.ms.button_main_energy_buy.GetComponentInChildren<Text>().text = (MainScript.ms.player.data.energy_max - MainScript.ms.player.data.energy) + " GOLD";
                MainScript.ms.button_main_energy_buy.SetActive(true);
            }
            else
                MainScript.ms.button_main_energy_buy.SetActive(false);
        }






    }
    public bool exit;

    public IEnumerator FullHP()
    {
        if (player.data.Heros.Count > 0)
        foreach (Hero heroes in player.data.Heros) heroes.heals = heroes.maxHp;

        yield return null;
    }

    public Coroutine timerGame;

    public Text PVPMoney;

    public string CurrentTime;

    public IEnumerator TimerEnd()
    {
        //print(Convert.ToDateTime(CurrentTime));
        //print(DateTime.Now.ToString("HH:mm:ss"));
        //print(Convert.ToDateTime("23:59:00") - Convert.ToDateTime (DateTime.Now.ToString("HH:mm:ss")) );
        //string hhOst = (Convert.ToDateTime("23:59:00").Hour - Convert.ToDateTime(CurrentTime).Hour).ToString();
        //string mmOst = (Convert.ToDateTime("23:59:00").Minute - Convert.ToDateTime(CurrentTime).Minute).ToString();
        //string ssOst = (Convert.ToDateTime("23:59:00").Second - Convert.ToDateTime(CurrentTime).Second).ToString();
        while (true)
        {
            if (exit)
            {
                break;
            }




            PVPMoney.text = "Сброс входа: "+ (Convert.ToDateTime("23:59:59") - Convert.ToDateTime(DateTime.Now.ToString("HH:mm:ss"))).ToString();

            yield return new WaitForSeconds(1f);
        }
    }

    public bool first = true;
    public IEnumerator timer()
    {
        
        int timer = 0;
        first = true;
        StartCoroutine(TimerEnd());
        while (true)
        {
            if (exit) break;
            timer++;

            if (timer % 5 == 1)
            {
                WWW www = new WWW(MainScript.ms.server_url + "get_login.php?login=" + player.login + " &pass=" + player.pass);
                while (!www.isDone) ;

                

                MainScript.ms.player.data.requestFriends = JsonUtility.FromJson<Player>(www.text).data.requestFriends;
                MainScript.ms.player.data.SendsRequests = JsonUtility.FromJson<Player>(www.text).data.SendsRequests;
                MainScript.ms.player.data.friends = JsonUtility.FromJson<Player>(www.text).data.friends;

                if (GameObject.Find("menu_chat_invites"))
                {
                    foreach (Transform n in GameObject.Find("menu_chat_invites").transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                    {
                        Destroy(n.gameObject);
                    }
                    foreach (requestFriend friend in player.data.requestFriends)
                    {
                            var asd = Instantiate(MainScript.ms.ItemFind, GameObject.Find("menu_chat_invites").transform.Find("Scroll View").Find("Viewport").Find("Content").transform);
                            asd.GetComponent<FindedPlayer>().id = friend.pid;
                            asd.GetComponent<FindedPlayer>().name = friend.name;
                            asd.GetComponent<FindedPlayer>().pass = friend.pass;
                            asd.transform.Find("Text").GetComponent<Text>().text = friend.name;
                            asd.name = friend.name;
                    }
                    foreach (Transform n in MainScript.ms.menu_chat_invites.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
                    {
                        if (MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text != "")
                        {
                            if (n.name.Contains(MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text))
                            {
                                n.gameObject.SetActive(true);
                            }
                            else
                            {
                                n.gameObject.SetActive(false);
                            }
                        }
                        else
                        {
                            n.gameObject.SetActive(true);
                        }
                    }
                }

                Player_data_save();
            }

            if (timer % 50 == 0)
            {

                if (MainScript.ms.player.data.energy < MainScript.ms.player.data.energy_max) MainScript.ms.player.data.energy++;

            }




            if (MainScript.ms.menu_chat_all.activeSelf && MainScript.ms.fon_chat.activeSelf)
            {
                if (timer % 2 == 1)
                {
                    var www = new WWW(MainScript.ms.server_url + "chat.php?user_id=1&to_id=0");
                    while (!www.isDone) ;
                    if (www.text != "null")
                    {
                        GameObject.Find("text_chat_all").GetComponent<Text>().text = "";
                        var chat_messages = JsonUtility.FromJson<Chat_messages>(www.text);

                        foreach (Chat_message cm in chat_messages.chat_messages)
                        {

                            GameObject.Find("text_chat_all").GetComponent<Text>().text += cm.login + ": " + cm.text + "\n";
                        }
                    }
                }
            }


            if (MainScript.ms.fon.activeSelf)
            {
                if (timer % 2 == 1)
                {

                    var www = new WWW(MainScript.ms.server_url + "chat.php?user_id=0&to_id=0");
                    while (!www.isDone) ;
                    if (www.text != "null")
                    {
                        GameObject.Find("text_chat").GetComponent<Text>().text = "";
                        var chat_messages = JsonUtility.FromJson<Chat_messages>(www.text);

                        GameObject.Find("text_chat").GetComponent<Text>().text += chat_messages.chat_messages[0].date + " - " + chat_messages.chat_messages[0].login + ": " + chat_messages.chat_messages[0].text + "\n";
                        GameObject.Find("text_chat").GetComponent<Text>().text += chat_messages.chat_messages[1].date + " - " + chat_messages.chat_messages[1].login + ": " + chat_messages.chat_messages[1].text;


                    }
                }


            }
            WWW currentTime = new WWW(server_url+"get_data.php");
            yield return currentTime;
            CurrentTime = currentTime.text.Substring(16, 21).Replace("\\", "").Substring(10).Trim();
            player.data.LastJoinDate = currentTime.text.Substring(16, 21).Replace("\\", "");

            //var dt = new DateTime(Convert.ToDateTime(currentTime.text.Substring(16, 21).Replace("/", "")).Year, Convert.ToDateTime(currentTime.text.Substring(16, 21).Replace("/", "")).Month, Convert.ToDateTime(currentTime.text.Substring(16, 21).Replace("/", "")).Day);
            //var cal = new GregorianCalendar();
            //var weekNumber = cal.GetWeekOfYear(dt, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
            //if (weekNumber % 2 == 0)
            //{
            //    buttonWeekly1.SetActive(false);
            //    buttonWeekly2.SetActive(false);
            //    buttonWeekly3.SetActive(true);
            //    buttonWeekly4.SetActive(true);
            //}
            //else
            //{
            //    buttonWeekly1.SetActive(true);
            //    buttonWeekly2.SetActive(true);
            //    buttonWeekly3.SetActive(false);
            //    buttonWeekly4.SetActive(false);
            //}

            if (player.data.UnlockMoneyDate != "")
                if (Convert.ToDateTime(player.data.LastJoinDate) > Convert.ToDateTime(player.data.UnlockMoneyDate))
                {
                    player.data.UnlockMoneyDate = "";
                    player.data.UnlockMoney = 50;

                }

            yield return new WaitForSeconds(1f);

            first = false;
            if (exit) break;
        }
    }

    public void FindFriends()
    {
        foreach (Transform n in MainScript.ms.menu_chat_friends.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
        {
            if (MainScript.ms.menu_chat_friends.transform.Find("input_name").GetComponent<InputField>().text != "")
            {
                if (n.name.Contains(MainScript.ms.menu_chat_friends.transform.Find("input_name").GetComponent<InputField>().text))
                {
                    n.gameObject.SetActive(true);
                }
                else
                {
                    n.gameObject.SetActive(false);
                }
            }
            else
            {
                n.gameObject.SetActive(true);
            }
        }
    }

    public void FindInvites()
    {
        foreach (Transform n in MainScript.ms.menu_chat_invites.transform.Find("Scroll View").Find("Viewport").Find("Content").transform)
        {
            if (MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text != "")
            {
                if (n.name.Contains(MainScript.ms.menu_chat_invites.transform.Find("input_name").GetComponent<InputField>().text))
                {
                    n.gameObject.SetActive(true);
                }
                else
                {
                    n.gameObject.SetActive(false);
                }
            }
            else
            {
                n.gameObject.SetActive(true);
            }
        }
    }

    public GameObject buttonWeekly1;
    public GameObject buttonWeekly2;
    public GameObject buttonWeekly3;
    public GameObject buttonWeekly4;

    public void ShowMessage(string title, string text, string button_text)
    {
        MainScript.ms.fon_message.gameObject.SetActive(true);
        GameObject.Find("text_title").GetComponent<Text>().text = title;
        GameObject.Find("text_message").GetComponent<Text>().text = text;
        GameObject.Find("button_message_exit").GetComponentInChildren<Text>().text = button_text;


    }
    public void ShowMessageClose()
    {
        MainScript.ms.fon_message.gameObject.SetActive(false);

    }
    public bool selected;
    public int Maxlvl;
    public int Minlvl;

    public static MainScript ms;
    public bool sound_on;
    public bool music_on;
    public string server_url = "http://megaxchange.site/";

    public GameObject TextDamagepref;

    public GameObject fon;
    public GameObject fon_settings;
    public GameObject fon_main;
    public GameObject fon_heroes;
    public GameObject fon_heroes_content;
    public GameObject fon_heroes_buy_window;

    public GameObject fon_login;
    public GameObject fon_message;
    public GameObject fon_heroes_one;
    public GameObject fon_shop_buy;
    public GameObject fon_shop;
    public GameObject fon_com1;

    public bool raiting = false;
    public GameObject fon_select_but1;
    public GameObject fon_select_but2;
    public GameObject fon_com_level;
    public GameObject fon_weekly;
    public string weekly_type = "0";

    public GameObject button_main_energy_buy;




    public GameObject fon_fight;
    public GameObject fon_fight_finder;
    public GameObject fon_fight_exit;
    public GameObject fon_fight_res;
    public GameObject fon_fight_res_lose;


    public GameObject fon_fight_prepare;

    public GameObject fon_select_heroes;


    public GameObject fon_invent;
    public GameObject fon_invent_use;
    public GameObject fon_chat;


    public int i_timer = 0;
    public int count_of_action = 0;

    public GameObject tree;
    public GameObject menu_chat_poisk;
    public GameObject menu_chat_all;
    public GameObject menu_chat_friends;
    public GameObject menu_chat_invites;
    public GameObject menu_chat_admin;

    public GameObject button_botfight;

    public GameObject Find_timer;


    int i = 0;
    public Player player, enemy;

    //fight section
    public int cankick_slider;
    public int cooldown = 100;
    public Skill_correction skill_correction = new Skill_correction();
    public Coroutine fight_curse_exec;
    public Fight internal_fight = new Fight();



    //heroes sectnion
    public List<int> lvl_up_val = new List<int> { 50, 100, 200, 400, 5000, 6000, 7000, 8000, 9000 };

    //heroes sectnion
    public List<int> player_lvl_up_val = new List<int> { 100, 200, 300, 900, 5000, 6000, 7000, 8000, 9000 };



    public void Make_Action(ref Player player, Action action)
    {


        //if (reverse)
        //{
        //    int damage = Convert.ToInt16(action.value) * 30 / 100;
        //    damage = Convert.ToInt32(damage);
        //    MainScript.ms.Send_action("kick", damage + "");
        //}

        if (action.name == "kick" && Get_enemy_alive_hero() != null && Get_player_alive_hero() != null)
        {
            if (enemy.isbot)
            {
                if (action.pid1 == 0)
                {
                    //player.data.Heros[MainScript.ms.player.data.Heros.IndexOf(Get_player_alive_hero())].heals -= Convert.ToInt32(action.value);

                    Get_player_alive_hero().heals -= Convert.ToInt32(action.value);
                    if (reverse)
                    {

                        int damage = Convert.ToInt16(action.value) * 30 / 100;
                        damage = Convert.ToInt32(damage * skill_correction.damage);
                        MainScript.ms.Send_action("kick", damage + "");
                        player.data.Heros[Get_alive_hero_id_byplayer(player)].heals -= Convert.ToInt32(action.value);
                    }
                }
                else
                {
                    Get_enemy_alive_hero().heals -= Convert.ToInt32(action.value);


                }

            }
            else
            {

                //player.data.Heros[Get_alive_hero_id_byplayer(player)].heals -= Convert.ToInt32(action.value);

                if (action.pid1 == player.pid)
                {

                    //enemy.data.Heros[MainScript.ms.enemy.data.Heros.IndexOf(Get_enemy_alive_hero())].heals -= Convert.ToInt32(action.value);
                    Get_enemy_alive_hero().heals -= Convert.ToInt32(action.value);


                }
                else
                {

                    // player.data.Heros[MainScript.ms.player.data.Heros.IndexOf(Get_player_alive_hero())].heals -= Convert.ToInt32(action.value);
                    Get_player_alive_hero().heals -= Convert.ToInt32(action.value);

                    if (reverse)
                    {

                        int damage = Convert.ToInt16(action.value) * 30 / 100;
                        damage = Convert.ToInt32(damage * skill_correction.damage);
                        MainScript.ms.Send_action("kick", damage + "");
                    }
                }
            }
        }
    }

    public string typeEnemyKick;
    public string LastClick;

    IEnumerator Fight_curse(string name, string NameMob)
    {
        int i = 0;
        int delta = Convert.ToInt32(10 * skill_correction.curse);
        //int delta = Convert.ToInt32 ( Convert.ToInt32(action.value)/10);
        //Send_action("curce","0");

        while (i < 10)
        {
            //  if (Get_enemy_alive_hero() != null && NameMob != Get_enemy_alive_hero().name)
            if (!fon_fight.transform.Find("fon_fight_exit").gameObject.activeSelf && enemy.isbot)
            {
                try
                {
                    if (NameMob != Get_enemy_alive_hero().name)
                    {
                        foreach (Transform n in ContentPlayer1.transform)
                        {
                            if (n.name.Contains(NameMob))
                            {
                                Destroy(n.gameObject);

                            }

                        }
                        break;
                    }
                }
                catch
                {
                    break;
                }


                Send_action("kick", delta + "");

                var a = Instantiate(MainScript.ms.TextDamagepref, fon_fight.transform);
                a.GetComponent<Text>().text = "" + delta;
                a.GetComponent<Text>().color = new Color(0.5f, 0, 0.5f);


                player.data.Heros.Find(x => x.name == name).damageAll += delta;
                i++;
            }
            yield return new WaitForSeconds(1f);

        }

    }

    IEnumerator reverseDamage()
    {

        reverse = true;
        yield return new WaitForSeconds(3f);

        reverse = false;

    }

    bool reverse = false;


    public IEnumerator Effect(GameObject Content, Sprite circle, Sprite color,string name)
    {
        Debug.Log("START");
        var a = Instantiate(PrefabEffect, Content.transform);
        a.transform.GetChild(0).GetComponent<Image>().sprite = circle;
        a.transform.GetChild(1).GetComponent<Image>().sprite = color;
        a.name = name;
        float i = 10;
        while (i > 0)
        {
          
            if (a == null) break;
            a.transform.GetChild(1).GetComponent<Image>().fillAmount = i / 10;
            i--;
            yield return new WaitForSeconds(1f);
        }
        Debug.Log("END");

        Destroy(a);

    }

    IEnumerator Skill_effect(int pid1, Action action)
    {

        Debug.Log("action name " + action.name);
        Debug.Log(" " + pid1);

        if (action.name.Contains("skill1")) //Ноил
        {
            if (player.pid == pid1)
            {
                //Send_action("skill1", "0");

                MainScript.ms.LastClick = MainScript.ms.Get_enemy_alive_hero().name;

                typeEnemyKick = Get_enemy_alive_hero().type;
                fight_curse_exec = StartCoroutine(Fight_curse("Ноил",LastClick));
                skill_correction.crit = 0;
                StartCoroutine(Effect(ContentPlayer1, circleNoil, Purple, "Noil"+ LastClick));
            }
            else
            {
                StartCoroutine(Effect(ContentPlayer2, circleNoil, Purple, "Noil" + LastClick));

            }
        }


        if (action.name.Contains("skill2")) //Рэй
        {
            if (player.pid == pid1)
            {
                Send_action("kick", 40 + "");
                var a = Instantiate(MainScript.ms.TextDamagepref, GameObject.Find("fon_fight").transform);
                a.GetComponent<Text>().color = new Color(0.02f, 1, 0.08f,1);
                player.data.Heros.Find(x => x.name == "Рэй").damageAll += 40;

            }
            else
            {
                skill_correction.curse = 2;
            }
        }


        //Маамун
        if (action.name.Contains("skill3"))
        {
            Debug.Log("j");
            if (player.pid == pid1)
            {
                StartCoroutine(Effect(ContentPlayer1, circleMaamyn, Green, "Maamyn"));
                Send_action("skill3", "0");

            }
            else
            {
                StartCoroutine(Effect(ContentPlayer2, circleMaamyn, Green, "Maamyn"));

                skill_correction.damage = 0;
                skill_correction.curse = 0;
                skill_correction.magic_damage = 0;

            }
        }



        if (action.name.Contains("skill4")) //Айна
        {
            if (player.pid == pid1)
            {
                fight_curse_exec = StartCoroutine(Fight_curse("Айна",LastClick));
                skill_correction.dodge = 0;
            }
            else
            {
                if (fight_curse_exec != null) StopCoroutine(fight_curse_exec);
            }
        }


        if (action.name.Contains("skill5")) //Деревянный
        {
            if (player.pid == pid1)
            {
                Send_action("skill4", "0");

                skill_correction.crit = 0;
                StartCoroutine(Effect(ContentPlayer1, circleDerevo, Green, "Derevo"));

            }
            else
            {
                StartCoroutine(Effect(ContentPlayer2, circleDerevo, Green, "Derevo"));

                skill_correction.magic_damage = 0;
            }
        }



        if (action.name.Contains("skill6")) //Лиа
        {
            if (player.pid == pid1)
            {
                Send_action("kick", "" + (40 * skill_correction.magic_damage));
                var a = Instantiate(MainScript.ms.TextDamagepref, GameObject.Find("fon_fight").transform);
                player.data.Heros.Find(x => x.name == "Лиа").damageAll += 40;
            }
            else
            {
                skill_correction.dodge = 0;
            }
        }




        if (action.name.Contains("skill7")) //Демон
        {
            if (player.pid == pid1)
            {
                Send_action("skill7", "0");

                StartCoroutine(reverseDamage());
                StartCoroutine(Effect(ContentPlayer1, circleDemon, Green, "Demon"));

            }
            else
            {
                StartCoroutine(Effect(ContentPlayer2, circleDemon, Green, "Demon"));

                skill_correction.magic_damage = 2;
            }
        }


        if (action.name.Contains("skill8")) //Призрак
        {
            if (player.pid == pid1)
            {
                Send_action("skill8", "0");

                StartCoroutine(Effect(ContentPlayer1, circleGhost, Green, "Ghost"));

            }
            else
            {
                StartCoroutine(Effect(ContentPlayer2, circleGhost, Green, "Ghost"));

                skill_correction.dodge = 2;
                skill_correction.curse = 2;
            }
        }


        if (action.name.Contains("skill9")) //Водный
        {
            if (player.pid == pid1)
            {
                Send_action("kick", (40 * skill_correction.damage) + "");
                var a = Instantiate(MainScript.ms.TextDamagepref, GameObject.Find("fon_fight").transform);
            }
            else
            {
                skill_correction.curse = 2;
            }
        }

        if (action.name.Contains("skill10")) //Каменный
        {
            if (player.pid == pid1)
            {
                Send_action("skill10", "0");

                StartCoroutine(Effect(ContentPlayer1, circleKamen, Red, "Kamen"));

            }
            else
            {
                StartCoroutine(Effect(ContentPlayer2, circleKamen, Red, "Kamen"));

                skill_correction.damage = 0.5f;
                skill_correction.magic_damage = 2;
            }
        }

        yield return new WaitForSeconds(10f);


        if (action.name.Contains("skill1")) //Ноил
        {
            if (player.pid == pid1)
            {
                skill_correction.crit = 1;
            }
        }

        if (action.name.Contains("skill2")) //Рэй
        {
            if (player.pid == pid1)
            {

            }
            else
            {
                skill_correction.curse = 1;
            }
        }

        //Маамун
        if (action.name.Contains("skill3"))
        {
            if (player.pid == pid1)
            {
            }
            else
            {
                skill_correction.damage = 1;
                skill_correction.curse = 1;
                skill_correction.magic_damage = 1;

            }
        }



        if (action.name.Contains("skill4")) //Айна
        {
            if (player.pid == pid1)
            {
                skill_correction.dodge = 1;
            }
        }


        if (action.name.Contains("skill5")) //Деревянный
        {
            if (player.pid == pid1)
            {
                skill_correction.crit = 1;
            }
            else
            {
                skill_correction.magic_damage = 1;
            }
        }



        if (action.name.Contains("skill6")) //Лиа
        {
            if (player.pid == pid1)
            {

            }
            else
            {
                skill_correction.dodge = 1;
            }
        }



        if (action.name.Contains("skill7")) //Демон
        {
            if (player.pid == pid1)
            {
            }
            else
            {
                skill_correction.magic_damage = 1;
            }
        }

        if (action.name.Contains("skill8")) //Призрак
        {
            if (player.pid == pid1)
            {
            }
            else
            {
                skill_correction.dodge = 1;
                skill_correction.curse = 1;
            }
        }



        if (action.name.Contains("skill9")) //Водный
        {
            if (player.pid == pid1)
            {

            }
            else
            {
                skill_correction.curse = 1;
            }
        }

        if (action.name.Contains("skill10")) //Каменный
        {
            if (player.pid == pid1)
            {

            }
            else
            {
                skill_correction.damage = 1;
                skill_correction.magic_damage = 1;
            }
        }


    }

    public bool[] givedCankick = { false, false, false };

    int damage_fight = 0;
    IEnumerator CheckAction()
    {
        ContentPlayer1.SetActive(true);
        ContentPlayer2.SetActive(true);
        damage_fight = 0;
        givedCankick[0] = false;
        givedCankick[1] = false;
        givedCankick[2] = false;

        foreach (Transform n in fon_fight.transform)
        {
            if (n.name.Contains("DamagePref"))
            {
                Destroy(n.gameObject);
            }
        }
            cankick_slider = 100;
            if (Get_enemy_alive_hero().type == "0")
            {
                GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s7/01");
            }
            else
            {
                GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s" + Get_enemy_alive_hero().type + "/01");
            }

            i = 0;
            var fight = Get_curent_fight_data();

            while (MainScript.ms.fon_fight.activeSelf && Get_enemy_alive_hero() != null && Get_player_alive_hero() != null)
            {
            if ((!fon_fight_finder.activeSelf && (enemy.isbot && !pause))|| (!fon_fight_finder.activeSelf && !enemy.isbot))
            {
                i++;


                if (enemy.isbot && i % 20 == 1 && !Find)
                {

                    Action action = new Action
                    {
                        name = "kick",
                        value = Get_enemy_alive_hero().damage + "",
                        pid1 = MainScript.ms.enemy.pid,
                        pid2 = MainScript.ms.player.pid
                    };
                    internal_fight.actions_list.Add(action);

                }



                if (i % 5 == 1)
                {
                    fight = Get_curent_fight_data();
                }

                int tmp_damage = 0;
                while (count_of_action < fight.actions_list.Count)
                {



                    if (fight.actions_list[count_of_action].pid1 == player.pid
                        && !fight.actions_list[count_of_action].name.Contains("skill")
                        && !fight.actions_list[count_of_action].name.Contains("leave")

                         )
                    {
                        Hero tmp_curent_enemy_hero = Get_enemy_alive_hero();

                        Make_Action(ref player, fight.actions_list[count_of_action]);
                        tmp_damage += Convert.ToInt32(fight.actions_list[count_of_action].value);

                        if (tmp_curent_enemy_hero != Get_enemy_alive_hero())
                        {
                            skill_correction = new Skill_correction();
                            if (fight_curse_exec != null) StopCoroutine(fight_curse_exec);
                            //cooldown = 100;
                        }

                    }
                    else
                    {
                        Make_Action(ref player, fight.actions_list[MainScript.ms.count_of_action]);
                    }

                    Debug.Log(fight.actions_list[MainScript.ms.count_of_action].name);

                    //обработчик скилов
                    if (fight.actions_list[MainScript.ms.count_of_action].name.Contains("skill"))
                    {
                        StartCoroutine(Skill_effect(fight.actions_list[MainScript.ms.count_of_action].pid1, fight.actions_list[MainScript.ms.count_of_action]));
                    }




                    if (fight.actions_list[MainScript.ms.count_of_action].name == "leave")
                    {
                        if (fight.actions_list[MainScript.ms.count_of_action].pid1 == MainScript.ms.player.pid)
                        {
                        }
                        else
                        {
                            MainScript.ms.fon_fight_res.gameObject.SetActive(true);
                            MainScript.ms.player.data.pobed++;
                            //MainScript.ms.fon_fight_res.transform.Find("text_fight_res_comment").GetComponent<Text>().text = "Противник позорно бежал!";
                            fon_fight_res_lose.transform.Find("Image").transform.Find("text_fight_res_comment").GetComponent<Text>().text = "получено опыта " + damage_fight;

                        }

                        Player_data_save();
                        WWW www = new WWW("");
                        if (raiting) www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=true" + "&status=closed");
                        else www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=false" + "&status=closed");
                        //new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass );
                    }





                    MainScript.ms.count_of_action++;

                }


                if (Get_player_alive_hero() != null)
                    Get_player_alive_hero().damageAll += tmp_damage;
                //player.data.Heros[MainScript.ms.player.data.Heros.IndexOf(Get_player_alive_hero())].damageAll += tmp_damage;
                player.data.damageAll += tmp_damage;




                //проигрышь

                if (Get_player_alive_hero() == null)

                {
                    ContentPlayer1.SetActive(false);
                    ContentPlayer2.SetActive(false);
                    foreach (Transform n in ContentPlayer1.transform)
                    {
                        Destroy(n.gameObject);
                    }

                    foreach (Transform n in ContentPlayer2.transform)
                    {
                        Destroy(n.gameObject);
                    }
                    GameObject.Find("PL1").transform.Find("slider_heals_player1").GetComponent<Slider>().value = 0;
                    GameObject.Find("PL1").transform.Find("slider_heals_player1").Find("Text").GetComponent<Text>().text = "0";

                    fon_fight_res_lose.gameObject.SetActive(true);
                    fon_fight_res_lose.transform.Find("Image").transform.Find("text_fight_res_comment").GetComponent<Text>().text = "получено опыта "+ damage_fight;
                    if (raiting) player.data.rating -= 15;

                    if (enemy.isbot) player.data.comicsPos[CurrentCom] = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        if (player.data.selected_heroes[i] != -1)
                        {
                            GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().color = new Color(0.19f, 0.19f, 0.19f, 0.9f);
                            MainScript.ms.player.data.comicsPos[MainScript.ms.CurrentCom - 1] = 0;
                            player.data.Heros.Find(x=> x.type == player.data.selected_heroes[i].ToString()).heals = player.data.Heros.Find(x => x.type == player.data.selected_heroes[i].ToString()).maxHp ;
                        }





                    }
                    MainScript.ms.GainMoney = false;



                }

                //выигрышь
                if (Get_enemy_alive_hero() == null)
                {
                    foreach(Transform n in Content_for_win.transform)
                    {
                        Destroy(n.gameObject);
                    }

                    if (weekly_type != "0" && !player.data.weekly_win[selected_weekly])
                    {
                        player.data.weekly_win[selected_weekly] = true;
                        if (selected_weekly == 0)
                        {
                            player.data.count_sheets += 1;
                            var a = Instantiate(ms.prefab_reward, Content_for_win.transform);
                            a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[2];
                            a.GetComponentInChildren<Text>().text = "x1";
                            MainScript.ms.Player_data_save();
                        }
                        if (selected_weekly == 1)
                        {
                            player.data.count_sheets += 2;
                            var a = Instantiate(ms.prefab_reward, Content_for_win.transform);
                            a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[2];
                            a.GetComponentInChildren<Text>().text = "x2";
                            MainScript.ms.Player_data_save();
                        }
                        if (selected_weekly == 2)
                        {
                            player.data.count_sheets += 3;
                            var a = Instantiate(ms.prefab_reward, Content_for_win.transform);
                            a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[2];
                            a.GetComponentInChildren<Text>().text = "x3";
                            MainScript.ms.Player_data_save();
                        }
                        if (selected_weekly == 3)
                        {
                            player.data.count_sheets += 5;
                            var a = Instantiate(ms.prefab_reward, Content_for_win.transform);
                            a.GetComponentInChildren<Image>().sprite = MainScript.ms.reward_icons[2];
                            a.GetComponentInChildren<Text>().text = "x5";
                            MainScript.ms.Player_data_save();
                        }
                    }

                    ContentPlayer1.SetActive(false);
                    ContentPlayer2.SetActive(false);

                    foreach (Transform n in ContentPlayer1.transform)
                    {
                        Destroy(n.gameObject);
                    }

                    foreach (Transform n in ContentPlayer2.transform)
                    {
                        Destroy(n.gameObject);
                    }

                    GameObject.Find("PL2").transform.Find("slider_heals_player2").GetComponent<Slider>().value = 0;
                    GameObject.Find("PL2").transform.Find("slider_heals_player2").Find("Text").GetComponent<Text>().text = "0";
                    fon_fight_res.gameObject.SetActive(true);
                    fon_fight_res.transform.Find("Image").Find("text_fight_res_comment").GetComponent<Text>().text = "получено опыта: "+ damage_fight;
                    player.data.pobed++;

                    if (GainMoney && !raiting && player.data.UnlockMoney > 0)
                    {
                        if (player.data.UnlockMoney == 50)
                            player.data.UnlockMoneyDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00).ToString();
                        player.data.UnlockMoney -= 1;
                        player.data.gold += 1;

                    }

                    if (raiting) player.data.rating += 10;
                    if (ComFight)
                    {
                        player.data.comiscKillBot[CurrentCom - 1] = true;
                        fon_com1.SetActive(true);
                        fon_com1.transform.Find("button_com_fight").gameObject.SetActive(false);
                        fon_com1.transform.Find("fon_com1_taptext").gameObject.SetActive(true);
                        ComFight = false;

                        if (CurrentCom == 4)
                        {
                            //fon_com1.transform.Find("button_com_next_page").gameObject.SetActive(true);
                        }
                    }

                    for (int i = 0; i < 3; i++)
                    {



                        // else if (player.data.Heros.Find(x => x.type == player.data.selected_heroes[i].ToString()).heals <= 0) GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().color = new Color(0.19f, 0.19f, 0.19f, 0.9f);
                        if (enemy.data.selected_heroes[i] != -1)
                            GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().color = new Color(0.19f, 0.19f, 0.19f, 0.9f);

                    }

                  

                    MainScript.ms.GainMoney = false;

                }



                if (cankick_slider < 100)
                {
                    if (cankick_slider >= 30) cankick_slider += 4;
                    if (cankick_slider < 30 && cankick_slider >= 5) cankick_slider += 2;
                    if (cankick_slider < 5) cankick_slider += 1;
                }

                if (cooldown < 100)
                {
                    cooldown += 5;
                }
                GameObject.Find("Cooldown").GetComponent<Slider>().value = cooldown;
            }

            for (int i = 0; i < 3; i++)
            {
                if (Get_player_alive_hero() != null)
                {
                    if (player.data.selected_heroes[i] == -1)
                        GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
                    // else if (player.data.Heros.Find(x => x.type == player.data.selected_heroes[i].ToString()).heals <= 0) 

                    else if (player.data.Heros[player.data.Heros.FindIndex(x => x.type == player.data.selected_heroes[i].ToString())].heals <= 0)
                    {
                        GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().color = new Color(0.19f, 0.19f, 0.19f, 0.9f);
                        if (!givedCankick[i])
                        {
                            givedCankick[i] = true;
                            cankick_slider = 100;
                            //cooldown = 100;
                        }
                    }
                }



                // else if (player.data.Heros.Find(x => x.type == player.data.selected_heroes[i].ToString()).heals <= 0) GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().color = new Color(0.19f, 0.19f, 0.19f, 0.9f);
                if (Get_enemy_alive_hero() != null)
                {
                    if (enemy.data.selected_heroes[i] == -1) GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
                    else if ((!enemy.isbot && enemy.data.Heros.Find(x => x.type == enemy.data.selected_heroes[i].ToString()).heals <= 0) || (enemy.isbot && enemy.data.Heros[enemy.data.selected_heroes[i]].heals <= 0))
                        GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().color = new Color(0.19f, 0.19f, 0.19f, 0.9f);
                }

                if (Get_enemy_alive_hero() != null && Get_enemy_alive_hero().type == "0")
                {
                    GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s7/01");
                }
                else if (Get_enemy_alive_hero() != null && Get_enemy_alive_hero().type != "0")
                {
                    GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s" + Get_enemy_alive_hero().type + "/01");
                }
                // GameObject.Find("her_for_bit").GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("animation/s" + Get_enemy_alive_hero().type + "/01");) 

            }




                if (Get_player_alive_hero() != null)
                {
                    GameObject.Find("slider_cankick_player1").GetComponent<Slider>().value = cankick_slider;

                    GameObject.Find("slider_heals_player1").GetComponent<Slider>().maxValue = Get_player_alive_hero().maxHp;
                    GameObject.Find("slider_heals_player1").GetComponent<Slider>().value = Get_player_alive_hero().heals;
                    GameObject.Find("slider_heals_player1").GetComponentInChildren<Text>().text = Get_player_alive_hero().heals + "/" + Get_player_alive_hero().maxHp;
                }

                if (Get_enemy_alive_hero() != null)
                {


                    GameObject.Find("slider_heals_player2").GetComponent<Slider>().maxValue = Get_enemy_alive_hero().maxHp;
                    GameObject.Find("slider_heals_player2").GetComponent<Slider>().value = Get_enemy_alive_hero().heals;
                    GameObject.Find("slider_heals_player2").GetComponentInChildren<Text>().text = Get_enemy_alive_hero().heals + "/" + Get_enemy_alive_hero().maxHp;
                }



               
                yield return new WaitForSeconds(0.2f);
            }

        
    }


    public Hero Get_enemy_alive_hero()
    {
        string ttt = "";
        for (int i = 0; i < 3; i++) if (enemy.data.selected_heroes[i] != -1)
            {
                ttt = Convert.ToString(enemy.data.selected_heroes[i]);
                if (!enemy.isbot)
                    if (enemy.data.Heros[enemy.data.Heros.FindIndex(x => x.type == ttt)].heals > 0)
                    {
                        return enemy.data.Heros[enemy.data.Heros.FindIndex(x => x.type == ttt)];
                    }

                if (enemy.isbot)
                    if (enemy.data.Heros[i].heals > 0)
                        return enemy.data.Heros[i];
            }

        return null;
    }

    public Hero Get_player_alive_hero()
    {
        string ttt = "";
        for (int i = 0; i < 3; i++)
            if (player.data.selected_heroes[i] != -1)
            {


                ttt = Convert.ToString(player.data.selected_heroes[i]);




                if (player.data.Heros[player.data.Heros.FindIndex(x => x.type == ttt)].heals > 0)
                {
                    return player.data.Heros[player.data.Heros.FindIndex(x => x.type == ttt)];
                }
            }
        return null;
    }


    public int Get_alive_hero_id_byplayer(Player player)
    {
        for (int i = 0; i < player.data.Heros.Count; i++) if (player.data.Heros[i].heals > 0) return i;
        return -1;
    }

    public void Send_action(string name, string value)
    {
        Action action = new Action
        {
            name = name,
            value = value,
            pid1 = MainScript.ms.player.pid,
            pid2 = MainScript.ms.enemy.pid
        };

        damage_fight += Convert.ToInt32(value);

        WWW www = new WWW("");
        // new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&action=" + JsonUtility.ToJson(action));
        if (!enemy.isbot)
            if (raiting) www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=true" + "&action=" + JsonUtility.ToJson(action));
        else www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=false" + "&action=" + JsonUtility.ToJson(action));

        else
            internal_fight.actions_list.Add(action);
    }




    public Fight Get_curent_fight_data()
    {

        if (!enemy.isbot)
        {
            WWW www = new WWW("");
            if (raiting) www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=true");
            else www = new WWW(MainScript.ms.server_url + "get_fight.php?login=" + MainScript.ms.player.login + "&pass=" + MainScript.ms.player.pass + "&enemy_id=-1" + "&raiting=false");
            while (!www.isDone) ;
            return JsonUtility.FromJson<Fight>(www.text);

        }
        else
            return internal_fight;


    }

    public IEnumerator StartFight()
    {
        int waitfight = 3;
        fon_fight_finder.SetActive(true);
        while (waitfight != 0)
        {
            yield return new WaitForSeconds(1);
            waitfight--;
            fon_fight_finder.GetComponentInChildren<Text>().text = "00:0" + waitfight;
        }
        Find = false;
        fon_fight_finder.GetComponentInChildren<Text>().text = "00:03";
        fon_fight_finder.SetActive(false);

    }

    public void Begin_fight()
    {

        StartCoroutine(StartFight());

        MainScript.ms.fon_fight.gameObject.SetActive(true);
        MainScript.ms.count_of_action = 0;

        for (int i = 0; i < 3; i++)
        {
            if (player.data.selected_heroes[i] != -1)
            {
                GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);

                GameObject.Find("fight_her" + (i + 1)).GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/her_ico" + player.data.selected_heroes[i]);
            }
            if (enemy.data.selected_heroes[i] != -1)
            {
                if (!enemy.isbot)
                {
                    GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                    GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/her_ico" + enemy.data.selected_heroes[i]);
                }
                else if (weekly_type != "0")
                {
                    GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                    GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/her_ico" + weekly_type);
                }
                else if (enemy.isbot)
                {
                    GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                    if (enemy.data.Heros[i].type == "0")
                    {
                        GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/her_ico7");

                    }
                    else
                    {
                        GameObject.Find("fight_eher" + (i + 1)).GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/her_ico" + enemy.data.Heros[i].type);
                    }
                }
            }
        }



        StartCoroutine(CheckAction());

        MainScript.ms.fon_fight.transform.Find("text_fight_player1").GetComponent<Text>().text = "" + player.login;
        MainScript.ms.fon_fight.transform.Find("text_fight_player2").GetComponent<Text>().text = "" + enemy.login;



    }






    private void Awake()
    {
        ms = this;
    }

    void Start()
    {


        sound_on = true;


        MainScript.ms.fon_select_but2.SetActive(false);
        MainScript.ms.fon_fight.gameObject.SetActive(false);
        MainScript.ms.fon_invent.gameObject.SetActive(false);
        MainScript.ms.fon_shop.gameObject.SetActive(false);
        MainScript.ms.fon_message.gameObject.SetActive(false);
        MainScript.ms.fon_heroes.gameObject.SetActive(false);
        MainScript.ms.fon_main.gameObject.SetActive(false);
        MainScript.ms.fon_settings.gameObject.SetActive(false);
        MainScript.ms.fon_com1.gameObject.SetActive(false);
        MainScript.ms.fon_chat.gameObject.SetActive(false);
        MainScript.ms.fon_fight_prepare.gameObject.SetActive(false);
        MainScript.ms.fon_login.gameObject.SetActive(true);

        lvl_up_val = new List<int> { 50, 100, 200, 400, 5000, 6000, 7000, 8000, 9000 };
        player_lvl_up_val = new List<int> { 100, 200, 300, 900, 5000, 6000, 7000, 8000, 9000 };


        //#if UNITY_EDITOR

        //        string login = "w1";
        //        string pass = "1";
        //        var www = new WWW(MainScript.ms.server_url + "get_login.php?login=" + login + " &pass=" + pass);
        //        while (!www.isDone) ;

        //        MainScript.ms.player = JsonUtility.FromJson<Player>(www.text);
        //        MainScript.ms.fon_login.SetActive(false);
        //        MainScript.ms.fon_main.SetActive(true);
        //        MainScript.ms.Fon_main_update();
        //        MainScript.ms.player.data.comix_position_index = 0;
        //#endif

        








    }
    // Update is called once per frame
    void Update()
    {

    }

}