﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDamageU : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(text());
    }
    public IEnumerator text()
    {
        while (gameObject.GetComponent<Text>().color.a > 0)
        {


            gameObject.transform.position = new Vector2(transform.position.x, transform.position.y + 5);
            gameObject.GetComponent<Text>().color = new Color(gameObject.GetComponent<Text>().color.r, gameObject.GetComponent<Text>().color.g, gameObject.GetComponent<Text>().color.b, gameObject.GetComponent<Text>().color.a-0.0125f);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}
