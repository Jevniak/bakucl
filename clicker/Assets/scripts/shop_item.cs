﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class shop_item : MonoBehaviour, IPointerClickHandler
{

    public Shop_item cur_shop_item;
    [SerializeField]
    private string art;

    [SerializeField]
    private Text cost;

    // Use this for initialization


    void Start () {
        SetParam();
    //    gameObject.GetComponent<Text>()

    }

	public void SetParam() {
     //   name.text = cur_shop_item.name;
        art = cur_shop_item.art;
        cost.text = cur_shop_item.cost+"";
    }

    public string name;
    public string type;
    public int lvl;
    public int heals;
    public int damage;
    public float crit_persent;
    public float dodge_persent;
    public int maxHp;
    public int damageAll;
    public string description;

    public void OnPointerClick(PointerEventData eventData)

    {
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().cur_shop_item = cur_shop_item;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().cost = cur_shop_item.cost;
        MainScript.ms.fon_heroes_buy.transform.Find("Image/her_image").GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/" + art.Replace("hero", "her_ico"));
        MainScript.ms.fon_heroes_buy.transform.Find("Image/her_image/LEVEL_HERO").GetComponent<Text>().text = "1";
        MainScript.ms.fon_heroes_buy.transform.Find("Image/her_name").GetComponent<Text>().text = cur_shop_item.name;

        if (art == "hero1")
        {

            name = "Ноил";
            type = "1";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
           // description = "Тип: Атака\\Проклятие\n\n На кинжалы Ноила наложено заклятие.При контакте с ними противник получает не только физический урон, а так же страдает от магического проклятия. \n\nНегативный эффект:  во время перезарядки умения, теряется возможность наносить критические удары;";

           

        }

        if (art == "hero2")
        {


            name = "Рэй";
            type = "2";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
               // description = "Тип: Атака\\Чистый урон\n\nРэй обладает особыми магическими кинжалами.  Удар с их использованием наносит противнику  чистый урон, от которого нельзя увернуться.\n\nНегативный эффект:  во время перезарядки умения, проклятия наносят повышенный урон.";

        }

        if (art == "hero3")
        {
           
                name = "Маамун";
            type = "3";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
               // description = "Тип: Поддержка\n\nМаамун способен создавать барьер, который полностью защищает от всех атак противника.\n\nНегативный эффект:  во время перезарядки умения, не работают способности связанные с восстановлением здоровья. ";
           
        }


        if (art == "hero4")
        {

            name = "Айна";
            type = "4";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
               // description = "Тип: Поддержка\n\nАйна обучалась в Олдиуме защитным техникам. Теперь она может развеивать многие негативные эффекты вражеских умений.\n\nНегативный эффект:  во время перезарядки умения, теряется возможность уворачиваться  от атак противника.";
         
        }

        if (art == "hero5")
        {

            name = "Деревянный";
            type = "5";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
              //  description = "Тип: Поддержка\n\nДеревянный элементаль умеет создавать барьер, который снижает урон от умений противников.\n\nНегативный эффект:  во время перезарядки умения, шанс нанести критический удар уменьшается.";
          
        }

        if (art == "hero6")
        {

            name = "Лиа";
            type = "6";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
               // description = "Тип: Магический урон\n\nИспользование умения Лии наносит большой магический урон по противнику. \n\nНегативный эффект:  во время перезарядки умения, теряется возможность уворачиваться от атак противника.";
        
        }

        if (art == "hero7")
        {

            name = "Демон";
            type = "7";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
              //  description = "Тип: Поддержка\n\nШипы Демона позволяют возвращать часть полученного урона противнику в виде физического урона. \n\nНегативный эффект:  во время перезарядки умения, магия наносит повышенный урон. ";
          
        }

        if (art == "hero8")
        {

            name = "Призрак";
            type = "8";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
              //  description = "Тип: Поддержка\n\nУмения Призрака повышает шанс увернуться от атак противника.\n\nНегативный эффект:  во время перезарядки умения проклятия наносят повышенный урон.";
           
        }

        if (art == "hero9")
        {

            name = "Водный";
            type = "9";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
            //description = "Тип: Атака\n\nВодный элементаль концентрируясь, способен нанести мощный удар, нанося физический урон.\n\nНегативный эффект:  во время перезарядки умения проклятия наносят повышенный урон.";
            
        }

        if (art == "hero10")
        {

            name = "Каменный";
            type = "10";
            lvl = 1;
            heals = 400;
            damage = 20;
            crit_persent = 0.5f;
            dodge_persent = 0.5f;
            maxHp = 400;
            damageAll = 0;
              //  description = "Тип: Поддержка\n\nКаменный элементаль способен создавать барьеры, которые снижают получаемый физический урон.\n\nНегативный эффект:  во время перезарядки умения магия наносит повышенный урон.";
           
        }

        MainScript.ms.fon_heroes_buy.transform.Find("Image/her_uron").GetComponent<Text>().text = "УРОН: "+damage;
        MainScript.ms.fon_heroes_buy.transform.Find("Image/her_krit").GetComponent<Text>().text = "КРИТ. УДАР: " + crit_persent +"%";
        MainScript.ms.fon_heroes_buy.transform.Find("Image/her_heals").GetComponent<Text>().text = "ЗДОРОВЬЕ " + heals;
        MainScript.ms.fon_heroes_buy.transform.Find("Image/button_buy/cost").GetComponent<Text>().text = ""+cur_shop_item.cost;
        MainScript.ms.fon_heroes_buy.transform.Find("Image/desc").GetComponent<Text>().text = MainScript.ms.all_description[System.Convert.ToInt32(type) - 1];

        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().name = name;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().type = type;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().lvl = lvl;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().heals = heals;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().damage = damage;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().crit_persent = crit_persent;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().dodge_persent = dodge_persent;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().maxHp = maxHp;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().damageAll = damageAll;
        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().description = description;

        MainScript.ms.fon_heroes_buy.GetComponent<ShopBuy>().objForDest = this.gameObject;


        MainScript.ms.fon_heroes_buy.SetActive(true);

        //        Debug.Log(cur_shop_item.name);
       


    }
}
