﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillTreeSelected : MonoBehaviour, IPointerClickHandler
{
    public string name;
   [Multiline]public string description;

    public void Start()
    {
        GameObject.Find("SkillImage").GetComponent<Image>().color = new Color(1, 1, 1, 0);
        GameObject.Find("DescriptionSkill").GetComponent<Text>().text = "";
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject.Find("SkillImage").GetComponent<Image>().color = new Color(1, 1, 1, 1);
        var skill = gameObject.GetComponent<SkillTreeSelected>();
        GameObject.Find("DescriptionSkill").GetComponent<Text>().text = skill.name + "\n" + skill.description;
        GameObject.Find("SkillImage").GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
    }
}
