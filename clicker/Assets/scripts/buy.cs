﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buy : MonoBehaviour
{
    public string art;
    public int cost;
    public Shop_item cur_shop_item;
    public void buy_item()
    {
        if (cost <= MainScript.ms.player.data.gold)
        {

            if (art == "hero1")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Ноил",
                    type = "1",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Атака\\Проклятие\n\n На кинжалы Ноила наложено заклятие.При контакте с ними противник получает не только физический урон, а так же страдает от магического проклятия. \n\nНегативный эффект:  во время перезарядки умения, теряется возможность наносить критические удары;"

                });

            }

            if (art == "hero2")
            {

                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Рэй",
                    type = "2",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Атака\\Чистый урон\n\nРэй обладает особыми магическими кинжалами.  Удар с их использованием наносит противнику  чистый урон, от которого нельзя увернуться.\n\nНегативный эффект:  во время перезарядки умения, проклятия наносят повышенный урон."

                });
            }

            if (art == "hero3")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Маамун",
                    type = "3",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Поддержка\n\nМаамун способен создавать барьер, который полностью защищает от всех атак противника.\n\nНегативный эффект:  во время перезарядки умения, не работают способности связанные с восстановлением здоровья. "
                });
            }


            if (art == "hero4")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Айна",
                    type = "4",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Поддержка\n\nАйна обучалась в Олдиуме защитным техникам. Теперь она может развеивать многие негативные эффекты вражеских умений.\n\nНегативный эффект:  во время перезарядки умения, теряется возможность уворачиваться  от атак противника."
                });
            }

            if (art == "hero5")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Деревянный",
                    type = "5",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Поддержка\n\nДеревянный элементаль умеет создавать барьер, который снижает урон от умений противников.\n\nНегативный эффект:  во время перезарядки умения, шанс нанести критический удар уменьшается."
                });
            }

            if (art == "hero6")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Лиа",
                    type = "6",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Магический урон\n\nИспользование умения Лии наносит большой магический урон по противнику. \n\nНегативный эффект:  во время перезарядки умения, теряется возможность уворачиваться от атак противника."
                });
            }

            if (art == "hero7")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Демон",
                    type = "7",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Поддержка\n\nШипы Демона позволяют возвращать часть полученного урона противнику в виде физического урона. \n\nНегативный эффект:  во время перезарядки умения, магия наносит повышенный урон. "
                });
            }

            if (art == "hero8")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Призрак",
                    type = "8",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Поддержка\n\nУмения Призрака повышает шанс увернуться от атак противника.\n\nНегативный эффект:  во время перезарядки умения проклятия наносят повышенный урон."
                });
            }

            if (art == "hero9")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Водный",
                    type = "9",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Атака\n\nВодный элементаль концентрируясь, способен нанести мощный удар, нанося физический урон.\n\nНегативный эффект:  во время перезарядки умения проклятия наносят повышенный урон."
                });
            }

            if (art == "hero10")
            {
                MainScript.ms.player.data.Heros.Add(new Hero
                {
                    name = "Каменный",
                    type = "10",
                    lvl = 1,
                    heals = 400,
                    damage = 20,
                    crit_persent = 0.5f,
                    dodge_persent = 0.5f,
                    maxHp = 400,
                    damageAll = 0,
                    description = "Тип: Поддержка\n\nКаменный элементаль способен создавать барьеры, которые снижают получаемый физический урон.\n\nНегативный эффект:  во время перезарядки умения магия наносит повышенный урон."
                });
            }


            MainScript.ms.player.data.gold -= cost;
            MainScript.ms.player.data.shop_items.Remove(cur_shop_item);

            StartCoroutine(MainScript.ms.UpdateInfo());

            Destroy(this.gameObject);


        }
        // 

        MainScript.ms.Player_data_save();
    }
    
}
