﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Hero_script : MonoBehaviour, IPointerClickHandler
{

    public Hero hero;
    public Text name;
    public Text damage;
    public Text lvl;
    public GameObject menu;
    public GameObject lvl_up;


    // Use this for initialization
    public void Update_hero()
    {

    }

    public void OnPointerClick(PointerEventData eventData)

    {

      

       // if (!MainScript.ms.fon_fight_prepare.activeSelf) { 
        MainScript.ms.fon_heroes_one.SetActive(true);
        Hero_about.self.Show_about(Convert.ToInt32(MainScript.ms.player.data.Heros.Find(x => x.type == hero.type).type));
      //  }
    //


    }


    void Start () {
        Debug.Log(gameObject.transform.parent.name + " : " + MainScript.ms.fon_fight_prepare.transform.Find("Scroll View").Find("Viewport").Find("Content").Find("fon_fight_prepare_heroes").name);

        if (gameObject.transform.parent.name == MainScript.ms.fon_fight_prepare.transform.Find("Scroll View").Find("Viewport").Find("Content").Find("fon_fight_prepare_heroes").name)
        {
            lvl.text = hero.lvl + "";
            this.GetComponent<Image>().sprite = Resources.Load<Sprite>("icons/her_ico" + hero.type);
        }
        else
        {
            name.text = hero.name;
            damage.text = MainScript.ms.lvl_up_val[hero.lvl - 1] - hero.damageAll + "";
            lvl.text = hero.lvl + "";


            if (MainScript.ms.lvl_up_val[hero.lvl] < hero.damageAll) hero.damageAll = MainScript.ms.lvl_up_val[hero.lvl];


            if (hero.damageAll == MainScript.ms.lvl_up_val[hero.lvl - 1]) lvl_up.SetActive(true);
            else lvl_up.SetActive(false);
            this.GetComponent<Image>().sprite = Resources.Load<Sprite>("her_ico" + hero.type);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
