﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLoad : MonoBehaviour
{
    public static ScriptLoad sl;
    void Start()
    {
        sl = this;
    }

    public IEnumerator loadTimer()
    {
        //MainScript.ms.timerGame = StartCoroutine(MainScript.ms.timer());
        StartCoroutine(time());
        StartCoroutine(MainScript.ms.timer());
        yield return null;
    }

    public IEnumerator time()
    {
        yield return new WaitForSeconds(0.001f);
        MainScript.ms.Player_data_save();
    }

    public IEnumerator UpdateInfo()
    {
        StartCoroutine(MainScript.ms.UpdateInfo());
        yield return null;
    }

}
